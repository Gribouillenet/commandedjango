<?php
class DB extends PDO {
<<<<<<< HEAD
  const DSN = 'mysql:host=localhost;dbname=commandesdjo';
  const USER = 'root';
  const PASSWORD = 'root';
=======
  const DSN = 'mysql:dbname=djangofrlkcmddjo;host=djangofrlkcmddjo.mysql.db';
  const USER = 'djangofrlkcmddjo';
  const PASSWORD = 's069YuatnABVfcc9';
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8

  public function __construct() {
    try {
      parent::__construct( self::DSN, self::USER, self::PASSWORD);
    }
    catch ( PDOException $e ) {
      die( 'Erreur : ' . $e->getMessage() );
    }
  }

  static public function select ( string $query, array $params = [] ) : array {
    $bdd = new DB;

    if ( $params ) {
      $req = $bdd->prepare( $query );
      $req->execute( $params );
    }
    else {
      $req = $bdd->query( $query );
    }

    $data = $req->fetchAll();

    return $data;
  }

  static public function update ( string $query, array $params = [] ) : int {
    $bdd = new DB;

    if ( $params ) {
      $req = $bdd->prepare( $query );
      $req->execute( $params );
    }
    else {
      $req = $bdd->query( $query );
    }

    $updated = $req->rowCount();

    return $updated;
  }

  static public function insert ( string $query, array $params = [] ) : int {
    $bdd = new DB;

    if ( $params ) {
      $req = $bdd->prepare( $query );
      $req->execute( $params );
    }
    else {
      $req = $bdd->query( $query );
    }

    $inserted = $req->rowCount();

    return $inserted;
  }

  static public function delete ( string $query, array $params = [] ) : int {
    $bdd = new DB;

    if ( $params ) {
      $req = $bdd->prepare( $query );
      $req->execute( $params );
    }
    else {
      $req = $bdd->query( $query );
    }

    $deleted = $req->rowCount();

    return $deleted;
  }
}
