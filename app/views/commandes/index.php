<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header.php'); ?>
<section class="page-inner clearfix">
		<header class="page-header d-flex flex-column">
			<h1>Confirmation de commande</h1>
			<?php $membre = $data['membre'];

		   		if($membre) :
	   		?>
	   		<aside class="infos-membre">


		   		<h2><?= $membre['radio_name'] ?></h2>
		   		<h3><?= $membre['name']?></h3>
		   		<ul>
			   		<li><?= $membre['tel_mob']?></li>
			   		<li><?= $membre['tel_fixe']?></li>
			   		<li>Email du commercial : <?= $membre['mail_member']?></li>
			   		<?php
				   	if( $membre['mail_delivery_member'] && !$membre['mail_livraison'] ){
						echo '<li>Email de livraison : <b>' . $membre['mail_delivery_member'] . '</b></li>';
					}
					elseif( $membre['mail_livraison'] && !$membre['mail_delivery_member'] ){
						echo '<li>Email de livraison : <b>' . $membre['mail_livraison'] . '</b></li>';
					}
			   		?>
		   		</ul>
	   		</aside>
	   		<? endif;?>
		</header>
<?php foreach( $data['commandes'] as $key => $commande ) :

	$radio_name = $commande['radio_name'];
	$commercial_name = $commande['name'];
	$commercial_mail = $commande['mail_member'];
	 $mail_member_delivery = $commande['mail_livraison'];
	$tel_fixe = $commande['tel_fixe'];
	$tel_mob = $commande['tel_mob'];
	$annonceur = $commande['annonceur'];
	$nom_campagne = $commande['nom_campagne'];
	$date_livraison_client = $commande['date_livraison_client'];
	$mail_livraison = $commande['mail_livraison'];
	//$mail_member_delivery = $commande['mail_member_delivery'];
	$type_voix = $commande['type_voix'];
	$interpretation = $commande['interpretation'];
	$duree = $commande['duree'];
	$style_musique = $commande['style_musique'];
	$style_message = $commande['style_message'];
	$interlocuteur = $commande['interlocuteur'];
	$tel1 = $commande['tel1'];
	$tel2 = $commande['tel2'];
	$mail_annonceur = $commande['mail_annonceur'];
	$observations = $commande['observations'];
	$elements_texte = $commande['elemts_texte'];
	$fichier = $commande['fichier'];
	$fichier_2 = $commande['fichier_2'];
	$fichier_3 = $commande['fichier_3'];
	$created_at = $commande['date_commande'];
	$idCommande = $commande[0];
	$ref_member = $commande['ref_Member'];
	$title_formules = $commande['title'];

// 	var_dump($commande['mail_member_delivery']);

	$adresse = "http://".$_SERVER['SERVER_NAME'];
	$_SESSION['adresse'] = $adresse;
// 	var_dump($_SERVER);

?>
<h2>Récapitulatif de votre commande</h2>
<form action="commandes/validation" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>

	<?php

		ob_start();


		if( $idCommande <= 9 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00000' .$idCommande . '</h4>';
		}
		elseif( $idCommande <= 99 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0000' . $idCommande . '</h4>' ;
		}
		elseif( $idCommande <= 999 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-000' . $idCommande . '</h4>';
		}
		elseif( $idCommande <= 9999 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00' . $idCommande . '</h4>';
		}
		elseif( $idCommande <= 99999 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0' . $idCommande . '</h4>';
		}
		elseif( $idCommande <= 999999 ){
			echo '<header class="header-cmd"><h4>RÉF. Djo-' . $ref_member . '-' . $created_at . '-' . $idCommande . '</h4>';
		}
		echo '<aside class="d-none" style="background-color:#EEEEEE; padding:15px;">
			<ul style="list-style:none;">
				<li>Radio : <b>' . $radio_name . '</b></li>
				<li>Commercial : <b>' . $commercial_name . '</b></li>
				<li>Email du commercial : <b>' . $commercial_mail . '</b></li>
				<li>Tél. fixe : <b>' . $tel_fixe . '</b></li>
				<li>Tél. mob : <b>' . $tel_mob . '</b></li>
			</ul>
			</aside>
		</header>
		<h3 class="title-formule-valid">' . $title_formules . '</h3>
		<ul class="list-item-cmd">
			<li>Annonceur : <b>' . $annonceur . '</b></li>
			<li>Campagne : <b>' . $nom_campagne . '</b></li>
			<li>Date de livraison souhaitée : <b>' . $date_livraison_client . '</b></li>
			<li>Email de livraison : <b>' . $mail_livraison . '</b></li>
	   </ul>
		<div class="fieldset-cmd">
			<h3>Style du message</h3>
			<ul class="list-item-cmd">
				<li>Voix : <b>' . $type_voix . '</b></li>
				<li>Interprétation : <b>' . $interpretation . '</b></li>
				<li>Durée (en seconde) : <b>'. $duree . '</b></li>';
		if($style_musique){
			echo '<li>Style de musique : <b>' . $style_musique . '</b></li>';
		}

		if($style_message){
			echo '<li>Style du message : <b>' . $style_message . '</b></li>';
		}
		echo '</ul></div>';
		if($interlocuteur){
			echo '<div class="fieldset-cmd">
			<h3>Validation finale de l\'annonceur</h3>
			<ul class="list-item-cmd">
				<li>Interlocuteur : <b>' . $interlocuteur . '</b></li>
				<li>Tél. principal : <b>' . $tel1 . '</b></li>
				<li>Tél. secondaire : <b>' . $tel2 . '</b></li>
				<li>Email : <b>' . $mail_annonceur . '</b></li>
			</ul>
		</div>';
		}
		if($observations){
			echo '<div class="fieldset-cmd">
			<h3>Observations</h3>
			<p>' . $observations . '</p>
		</div>';
		}
		if($elements_texte){
			echo '
		<div class="fieldset-cmd">
			<h3>Éléments du texte</h3>
			<p>' . $elements_texte . '</p
		</div>';
		}
		if($fichier){

			echo '<div class="fieldset-cmd"><h3>Fichiers fournis</h3><ul class="list-item-cmd">';

			if($fichier){

				echo '<li><a href="' . $adresse . '/public/files/' . $fichier . '" title="télécharger le fichier">' . $fichier . '</a></li><br>';
			}
			if($fichier_2){
				echo '<li><a href="' . $adresse . '/public/files/' . $fichier_2 . '" title="télécharger le fichier">' . $fichier_2 . '</a></li><br>';
			}
			if($fichier_3){
				echo '<li><a href="' . $adresse . '/public/files/' . $fichier_3 . '" title="télécharger le fichier">' . $fichier_3 . '</a></li>';
			}

			echo '</ul></div>';
		}

		$html = ob_get_contents();
		ob_end_flush();

		$message = preg_replace('#<div class="container">(.*)</div>#', '$1', $html);

	?>

	<textarea class="commande-finale d-none" name="commande">

		<?php echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Votre bon de commande</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #2A374E; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head><body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;">

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:rgb(255, 255, 255)" >
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content"><td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580"><h1 style="color:#841A59;">Bon de commande</h1>' . $message . '

                                                <p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>

                                                </td>
                                                 <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
	</body>
	</html>'; ?>
	</textarea>
<div class="btn btn-cmde btn-modif-commande">
	<a class="link-modif-commande" href="/commandes/modifier/<?= $idCommande ?>" title="Modifier la commande"><span>Modifier avant de commander</span></a>
</div>
<<<<<<< HEAD
<div class="btn btn-cmde btn-commande"><input type="submit" value="Commander"></div>
=======
<div class="btn btn-cmde btn-commande"><input type="submit" value="Confirmer la commander"></div>
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
</form>


<?php endforeach ;?>

	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
