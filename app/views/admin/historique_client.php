<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php');
    foreach( $data['account_membre'] as $key => $membre ) :

			$ref_member = ($membre['ref_Member'])?$membre['ref_Member']:$membre['id'];
			$radio_name = $membre['radio_name'];
			$name = $membre['name'];
			$tel_fix = $membre['tel_fixe'];
			$tel_mob = $membre['tel_mob'];
			$mail = $membre['mail_member'];
			$login = $membre['login'];
			$mail_delivery = $membre['mail_delivery_member'];

	endforeach;
?>
<section class="page-inner clearfix">
    <header class="page-header d-flex flex-column">
		<h1 class="text-xs-center">Historique du client</h1>
	    <div class="row">
		    <div class="col-md-6">
			    <aside class="infos-membre">
			   		<h2><?= $radio_name ?></h2>
			   		<ul>
				   		<li>Réf. client : <strong>Djo-<?= $ref_member ?></strong></li>
				   		<li><h3><?= $name ?></h3></li>
				   		<li><?= $tel_mob ?> <?php if( $tel_fix ) : ?> | <?= $tel_fix ?> <?php endif; ?></li>
				   		<li>Email commercial : <?= $mail ?></li>
				   		<li>Email de livraison : <?= $mail_delivery?></li>
			   		</ul>
		   		</aside>
		    </div>
	    </div>
    </header>
        <div class="container-list-commandes">
            <ul class="list-commandes" id="pagger-list-commandes">
                <?php
                if ($data['account_commandes']) {

                    foreach( $data['account_commandes'] as $key => $commandes ) :

                        $ref_member = $commandes['ref_Member'];
                        $annonceur = $commandes['annonceur'];
                        $nom_campagne = $commandes['nom_campagne'];
                        $created_at = $commandes['date_commande'];
                        $idCommande = $commandes[0];
                        $formules = $commandes['ref_Formules'];
                        $formules = $commandes['title'];
                        $date = $created_at;
                        $date = wordwrap($date,2,"/",1);

                    if( $commandes ) : ?>

                    <li class="item-commande line-item-<?= $idCommande ?>">

                    <article class="line-cmd d-flex justify-content-between">
                        <div class="infos-cmd">
                            <p>Commande du : <b><?= $date ?></b></p>
                        <?php

                            if( $idCommande <= 9 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        elseif( $idCommande <= 99 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        elseif( $idCommande <= 999 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        elseif( $idCommande <= 9999 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        elseif( $idCommande <= 99999 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        elseif( $idCommande <= 999999 ){
                            echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                        }
                        ?>
                            <h3><?= $annonceur?></h3>
                            <h2><?= $nom_campagne ?></h2>
                        </div>
                        <div class="link-spr">
                            <a class="link-delete" href="/admin/supprimer_hist/<?= $idCommande ?>" title="Supprimer" onclick="return confirm('Voulez-vous vraiment supprimer cet enregistrement ?')"><span>Supprimer</span></a>
                        </div>
                    </article>

                    </li>
                    <?php endif; endforeach;?>
                <?php } else { ?>
                    <li><h2>Le client n'a pas encore passé de commande.</h2></li>
                <?php } ?>
            </ul>
        </div>
       <a href="/admin/membres" class="link-come-back"><button>Retour</button></a>
      	</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer_compte.php'); ?>
