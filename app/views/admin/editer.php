<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin.php'); ?>
	<section class="page-inner clearfix">
		<header class="page-header">
	      <h1>Éditer une formule</h1>
		</header>

          <?php if ( isset( $data['erreur']['title'] ) || isset( $data['erreur']['body'] ) ) { ?>
            <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
          <?php } else { ?>
            <span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
          <?php } ?>

	      <form name="form1" id="form1" class="form-label-admin" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate onkeypress="refuserToucheEntree(event)">
            <label>Nom du projet*</label>
	        <input type="text" name="title" class="form-control form-obligatoire" placeholder="Nom du projet" value="<?= $data['formules']['title'] ?>">
	        <?php if ( isset( $data['erreur']['title'] ) ) : ?>
	            <div class="alert alert-danger alert-form"><?= $data['erreur']['title'] ?></div>
	        <?php endif; ?>
	        <label>Texte du projet*</label>
	        <textarea name="body" class="form-control form-obligatoire" placeholder="Texte du projet"><?= $data['formules']['body'] ?></textarea>
	        <?php if ( isset( $data['erreur']['body'] ) ) : ?>
	            <div class="alert alert-danger alert-form"><?= $data['erreur']['body'] ?></div>
	        <?php endif; ?>
	        <input type="submit" class="btn btn-success" value="Enregistrer">
	      </form>
	      <div class="col-12 pl-0"><a href="/admin" class="link-come-back"><button>Retour</button></a></div>
   </section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
