<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin.php'); ?>
	<section class="page-inner clearfix">
		<header class="page-header">
			<h1 class="text-xs-center">Gestion des formules</h1>
		</header>
	    <div class="row">
		    <h2>Ajouter ou modifier une formule</h2>

            <?php if ( isset( $data['erreur']['title'] ) || isset( $data['erreur']['body'] ) ) { ?>
                <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
            <?php } else { ?>
                <span class="champ-obligatoire d-inline-block col-12">* Champs Obligatoires</span>
            <?php } ?>

	        <div class="col-md-6">
	          <form name="form1" id="form1" action="/admin" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate onkeypress="refuserToucheEntree(event)">
	            <input type="text" name="title" class="form-control" placeholder="Titre de la formule*" value="<?php if ( isset( $_POST['title'] ) ) echo $_POST['title'] ?>">
	            <?php if ( isset( $data['erreur']['title'] ) ) : ?>
	                <div class="alert alert-danger"><?= $data['erreur']['title'] ?></div>
	            <?php endif; ?>
	            <textarea name="body" class="form-control" placeholder="Descriptif de la formule*"><?php if ( isset( $_POST['body'] ) ) echo $_POST['body'] ?></textarea>
	            <?php if ( isset( $data['erreur']['body'] ) ) : ?>
	                <div class="alert alert-danger"><?= $data['erreur']['body'] ?></div>
	            <?php endif; ?>
	            <div class="btn btn-valid"><input type="submit" value="Publier"></div>
	          </form>
	        </div>
	        <div class="col-md-6 bootstrap-table">
	            <table class="table table-striped">
<!--
                    <thead class="table-header mobile-off">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Titre</th>
                        <th scope="col" class="th-center">Éditer</th>
                        <th scope="col" class="th-center">Supprimer</th>
                      </tr>
                    </thead>
-->
                    <tbody>
                      <?php
                      foreach($data['formules'] as $formules) :
                      ?>
                      <tr>
                        <th scope="row">Ref-<?= $formules['id'] ?></th>
                        <td><?= $formules['title'] ?></td>
                        <td><a href="/admin/editer/<?= $formules['id'] ?>" class="edit"><span>Éditer</span></a></td>
                        <td><a href="/admin/supprimer/<?= $formules['id'] ?>" class="delete" onclick="return confirm('Voulez-vous vraiment supprimer cet enregistrement ?')"><span>Supprimer</span></a></td>
                      </tr>
                      <?php
                      endforeach;
                      ?>
                    </tbody>
	            </table>
	        </div>
	    </div>
	</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
