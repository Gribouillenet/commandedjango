<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header">
        <h1>Gestion des clients</h1>
    </header>
    <div class="row">
	   <h2>Ajouter ou modifier un client</h2>
	   <?php if ( isset( $data['erreur']['radio_name'] ) || isset( $data['erreur']['name'] ) || isset( $data['erreur']['tel_fixe'] ) || isset( $data['erreur']['tel_mob'] ) || isset( $data['erreur']['mail_member'] ) || isset( $data['erreur']['mail_delivery_member'] ) || isset( $data['erreur']['login'] ) ) { ?>
            <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
        <?php } else { ?>
            <span class="champ-obligatoire d-inline-block col-12">* Champs Obligatoires</span>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-4">
            <form name="form1" id="form1" action="/admin/membres" method="post" enctype="multipart/form-data" novalidate onkeypress="refuserToucheEntree(event)">
                <input type="text" name="radio_name" class="form-control form-obligatoire" placeholder="Nom de la radio*" value="<?php if ( isset( $_POST['radio_name'] ) ) echo $_POST['radio_name'] ?>">
                <?php if ( isset( $data['erreur']['radio_name'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['radio_name'] ?></div>
                <?php endif; ?>
                <input type="text" name="name" class="form-controlform-obligatoire" placeholder="Nom et Prénom du commercial*" value="<?php if ( isset( $_POST['name'] ) ) echo $_POST['name'] ?>">
                <?php if ( isset( $data['erreur']['name'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['name'] ?></div>
                <?php endif; ?>
                <input type="text" name="tel_fixe" class="form-control form-obligatoire" placeholder="Téléphone fixe*" value="<?php if ( isset( $_POST['tel_fixe'] ) ) echo $_POST['tel_fixe'] ?>">
                <?php if ( isset( $data['erreur']['tel_fixe'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['tel_fixe'] ?></div>
                <?php endif; ?>
                <input type="text" name="tel_mob" class="form-control form-obligatoire" placeholder="Téléphone mobile*" value="<?php if ( isset( $_POST['tel_mob'] ) ) echo $_POST['tel_mob'] ?>">
                <?php if ( isset( $data['erreur']['tel_mob'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['tel_mob'] ?></div>
                <?php endif; ?>
                <input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email du commercial*" value="<?php if ( isset( $_POST['mail_member'] ) ) echo $_POST['mail_member'] ?>">
                <?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['mail_member'] ?></div>
                <?php endif; ?>
                <input type="text" name="mail_delivery_member" class="form-control form-obligatoire" placeholder="Email de livraison*" value="<?php if ( isset( $_POST['mail_delivery_member'] ) ) echo $_POST['mail_delivery_member'] ?>">
                <?php if ( isset( $data['erreur']['mail_delivery_member'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['mail_delivery_member'] ?></div>
                <?php endif; ?>
                <input type="text" name="login" class="form-control form-obligatoire" placeholder="Identifiant*" value="<?php if ( isset( $_POST['login'] ) ) echo $_POST['login'] ?>">
                <?php if ( isset( $data['erreur']['login'] ) ) : ?>
                    <div class="alert alert-danger"><?= $data['erreur']['login'] ?></div>
                <?php endif; ?>
                <div class="btn btn-valid" ><input type="submit" value="Enregistrer"></div>
            </form>
        </div>
        <div class="col-md-8 bootstrap-table">
            <table class="table table-striped">
<!--
                <thead class="table-header mobile-off">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Éditer</th>
                    <th scope="col" class="th-center">Générer un nouveau mot de passe</th>
                    <th scope="col" class="th-center">Supprimer</th>
                  </tr>
                </thead>
-->
                <tbody>
                  <?php
                  foreach($data['member'] as $member) :
                  ?>
                  <tr>
                    <th>Djo-<?= $member['id'] ?></th>
                    <td><?= $member['name'] ?></td>
                    <td><a href="/admin/historique_client/<?= $member['id'] ?>" class="hist"><span>Historique</span></a></td>
                    <td><a href="/admin/editer_client/<?= $member['id'] ?>" class="edit"><span>Éditer</span></a></td>
                    <td><a href="/admin/password/<?= $member['id'] ?>" class="new-password"><span>Générer</span></a></td>
                    <td><a href="/admin/supprimer_client/<?= $member['id'] ?>" class="delete" onclick="return confirm('Voulez-vous vraiment supprimer ce client ?')"><span>Supprimer</span></a></td>
                  </tr>
                  <?php
                  endforeach;
                  ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
