<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin.php'); ?>

      <h1 class="text-xs-center">member</h1>
      <?php if ( isset( $data['erreur']['password'] ) ) : ?>
            <div class="alert alert-danger"><?= $data['erreur']['password'] ?></div>
          <?php endif; ?>
           
          <form action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate>
 
			<input type="password" name="password" class="form-control" placeholder="nouveau password" value="">
            
			
			<input type="submit" class="btn btn-success" value="Enregistrer">
          </form>
      <a href="//commandes-djo.gribdev.eu/admin/membres" class="navbar-brand"><button>Retour sur la page d'édition</button></a>
    </div>
  </body>
</html>
