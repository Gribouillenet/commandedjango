<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_admin_membres.php'); ?>
<section class="page-inner clearfix">
    <header class="page-header">
        <h1>Éditer une fiche client</h1>
    </header>

    <?php if ( isset( $data['erreur']['radio_name'] ) || isset( $data['erreur']['name'] ) || isset( $data['erreur']['tel_fixe'] ) || isset( $data['erreur']['tel_mob'] ) || isset( $data['erreur']['mail_member'] ) || isset( $data['erreur']['mail_delivery_member'] ) || isset( $data['erreur']['login'] ) ) { ?>
        <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
    <?php } else { ?>
        <span class="champ-obligatoire d-inline-block col-12">* Champs Obligatoires</span>
    <?php } ?>

    <form name="form1" id="form1" action="" method="post" class="form-fiche-client" enctype="multipart/form-data" novalidate onkeypress="refuserToucheEntree(event)">
		<label for="radio_name">Nom de la radio*</label>
		<input type="text" name="radio_name" class="form-control form-obligatoire" placeholder="Nom de la radio" value="<?php echo $data['member']['radio_name'];?>">
		<?php if ( isset( $data['erreur']['radio_name'] ) ) : ?>
            <div class="alert alert-danger"><?= $data['erreur']['radio_name'] ?></div>
        <?php endif; ?>
		<label for="name">Nom et Prénom du commercial*</label>
		<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom et Prénom du commercial" value="<?php echo $data['member']['name'];?>">
		<?php if ( isset( $data['erreur']['name'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['name'] ?></div>
		<?php endif; ?>
		<label for="tel_fixe">Téléphone fixe*</label>
		<input type="text" name="tel_fixe" class="form-control form-obligatoire" placeholder="Téléphone fixe" value="<?php echo $data['member']['tel_fixe']; ?>">
		<?php if ( isset( $data['erreur']['tel_fixe'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['tel_fixe'] ?></div>
		<?php endif; ?>
		<label for="tel_mob">Téléphone mobile*</label>
		<input type="text" name="tel_mob" class="form-control form-obligatoire" placeholder="Téléphone mobile" value="<?php echo $data['member']['tel_mob']; ?>">
		<?php if ( isset( $data['erreur']['tel_mob'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['tel_mob'] ?></div>
		<?php endif; ?>
		<label for="mail_member">Email du commercial*</label>
		<input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email" value="<?php echo $data['member']['mail_member']; ?>">
		<?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['mail_member'] ?></div>
		<?php endif; ?>
		<label for="mail_delivery_member">Email de livraison*</label>
		<input type="text" name="mail_delivery_member" class="form-control form-obligatoire" placeholder="Email de livraison" value="<?php echo $data['member']['mail_delivery_member']; ?>">
		<?php if ( isset( $data['erreur']['mail_delivery_member'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['mail_delivery_member'] ?></div>
		<?php endif; ?>
		<label for="login">Identifiant*</label>
		<input type="text" name="login" class="form-control form-obligatoire" placeholder="Identifiant" value="<?php echo $data['member']['login'];  ?>">
        <?php if ( isset( $data['erreur']['login'] ) ) : ?>
			<div class="alert alert-danger"><?= $data['erreur']['login'] ?></div>
		<?php endif; ?>

        <input type="submit" class="btn btn-success" value="Enregistrer">
    </form>
    <div class="col-12 pl-0"><a href="/admin/membres" class="link-come-back"><button>Retour</button></a></div>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
