<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Django Studio - Vos identifiants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #F2F0F0; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
    p{margin: 15px 0;}

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;" style="background-color:#F2F0F0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#F2F0F0;">
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
			                    <td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
	                                                <p style="margin: 15px 0;">Bonjour,<br><br>Vous nous avez demander de vous générer un nouveau mot de passe.</p>
	                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre nouveau mot de passe pour vous connecter à l\'utilitaire.</p>
	                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
		                                                <a href="https://commandes.django.fr" title="Découvrir l\'utilitaire de commandes Django Studio" target="_blank" style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Commandes - Django Studio</button>
			                                            </a>
			                                        </p>
			                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
			                                        <p style="margin: 0 0 15px;">Votre identifiant : <b> <?php echo $member[0]['login']; ?></b></p>
			                                        <p style="margin: 15px 0;">Votre mot de passe : <b><?php echo $password2 ?><b></p>
			                                        <p style="margin: 15px 0">Merci de votre confiance.<br>L'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>
			                                    </td>
                                                <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
</body>
</html>