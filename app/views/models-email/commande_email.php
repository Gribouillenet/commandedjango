<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Votre bon de commande</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #2A374E; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head><body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;">

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:rgb(255, 255, 255)" >
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
		                     	<td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
		                            <h1>Bon de commande</h1>
		                            <header class="header-cmd">
			                            <h4>RÉF. Djo-1-090718-000096</h4>
			                            <aside class="d-none" style="background-color:#EEEEEE; padding:15px;">

			                            <ul style="list-style: none; padding: 0px; margin: 0px;">
			                            <li>Radio : <b>Radio NRJ</b></li>
			                            <li>Commercial : <b>Fabrice Arnauld</b></li>
			                            <li>Email : <b>fabricearnauld@gmail.com</b></li>
				                           <li>Tél. fixe : <b>0142871415</b></li>
				                            <li>Tél. mob : <b>0695661213</b></li>
				                         </ul>
				                            </aside>
				                            </header>

				                            <ul class="list-item-cmd">
					                            <li>Formule : <b>Prêt à Diffuser</b></li>
					                            <li>Annonceur : <b>audi</b></li><li>Campagne : <b>nouvelle A4</b></li><li>Date de livraison souhaitée : <b>19/09/2018</b></li><li>Email de livraison : <b>audi@gmail.com</b></li></ul><div class="fieldset-cmd"><h3>Style du message</h3><ul class="list-item-cmd"><li>Voix : <b>homme</b></li><li>Interprétation : <b>sérieuse</b></li><li>Durée (en seconde) : <b>15</b></li><li>Style de musique : <b>classique</b></li>
						                            <li>Style du message : <b>heureux</b></li></ul></div><div class="fieldset-cmd"><h3>Validation finale de l'annonceur</h3><ul class="list-item-cmd"><li>Interlocuteur : <b>Paul Durand</b></li><li>Tél. principal : <b>02 00 00 0</b></li><li>Tél. secondaire : <b>06 00 00 0</b></li><li>Email : <b></b></li></ul></div><div class="fieldset-cmd"><h3>Observations</h3><ul class="list-item-cmd"><li><p>observation au projet</p></li></ul></div><div class="fieldset-cmd"><h3>Fichiers fournis</h3><ul class="list-item-cmd"><li><a href="http://commandes-djo.gribdev.eu/public/files/cmd-Djo--090718-pret-commande-1-nouvelle-A4-96.PDF" title="télécharger le fichier">cmd-Djo--090718-pret-commande-1-nouvelle-A4-96.PDF</a></li><br></ul></div>
                                                </td>
                                                 <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes-djo.gribdev.eu/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>