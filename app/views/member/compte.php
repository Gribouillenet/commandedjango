<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_compte.php');

	foreach( $data['account_membre'] as $key => $membre ) :

			$ref_member = ($membre['ref_Member'])?$membre['ref_Member']:$membre['id'];
			$radio_name = $membre['radio_name'];
			$name = $membre['name'];
			$tel_fix = $membre['tel_fixe'];
			$tel_mob = $membre['tel_mob'];
			$mail = $membre['mail_member'];
			$login = $membre['login'];
			$mail_delivery = $membre['mail_delivery_member'];

	endforeach;
?>
<section class="page-inner clearfix">
	<header class="page-header d-flex flex-column">
		<h1 class="text-xs-center">Mon Compte</h1>
	    <div class="row">
		    <div class="col-md-6">
			    <aside class="infos-membre">
			   		<h2><?= $radio_name ?></h2>
			   		<ul>
				   		<li>Réf. client : <strong>Djo-<?= $ref_member ?></strong></li>
				   		<li><h3><?= $name ?></h3></li>
				   		<li><?= $tel_mob ?> <?php if( $tel_fix ) : ?> | <?= $tel_fix ?> <?php endif; ?></li>
				   		<li>Email commercial : <?= $mail ?></li>
				   		<li>Email de livraison : <?= $mail_delivery?></li>
			   		</ul>
		   		</aside>
		    </div>
	    	<div class="col-md-6 d-flex flex-column justify-content-md-end align-items-end">
				<ul class="nav-infos-membre infos-membre">
	 			    <li class="nav-item-account"><a class="nav-link-account link-edit-profil d-flex justify-content-end" href="/member/editer"><span>Éditer son profil</span></a></li>
				    <li class="nav-item-account"><a id="link-historic-commande" class="nav-link-account link-historic-commande d-flex justify-content-end" data-toggle="link-hist-button" data-target="#autre"><span>Gestions des commandes</span></a></li>
		        </ul>
	    	</div>
	    </div>
		</header>
	<div class="container-list-commandes">
		<ul class="list-commandes" id="pagger-list-commandes">
			<?php
            if ($data['account_commandes']) {

                foreach( $data['account_commandes'] as $key => $commandes ) :

                    $annonceur = $commandes['annonceur'];
                    $nom_campagne = $commandes['nom_campagne'];
                    $created_at = $commandes['date_commande'];
                    $idCommande = $commandes[0];
                    $formules = $commandes['ref_Formules'];
                    $formules = $commandes['title'];
                    $date = $created_at;
                    $date = wordwrap($date,2,"/",1);

                if( $commandes ): ?>

                <li class="item-commande line-item-<?= $idCommande ?>">

                <article class="line-cmd d-flex justify-content-between">
                    <div class="infos-cmd col-8">
                        <p>Commande du : <b><?= $date ?></b></p>
                    <?php

                        if( $idCommande <= 9 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    elseif( $idCommande <= 99 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    elseif( $idCommande <= 999 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-000' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    elseif( $idCommande <= 9999 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-00' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    elseif( $idCommande <= 99999 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-0' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    elseif( $idCommande <= 999999 ){
                        echo '<aside>RÉF. Djo-' . $ref_member . '-' . $created_at . '-' . $idCommande . ' <sep>|</sep> <span>' . $formules . '</span></aside>';
                    }
                    ?>
                        <h3><?= $annonceur?></h3>
                        <h2><?= $nom_campagne ?></h2>
                    </div>
                    <div class="link-hist-button col-4 d-none">
                       <div class="row">
                            <div class="link-rcmd col-6">
                                <a class="link-renew" href="/commandes/editer/<?= $idCommande ?>" title="Re-commander"><span class="col-12 p-0">Re-commander</span></a>
                            </div>
                            <div class="link-rcmd link-spr col-6">
                                <a class="link-delete" href="/commandes/supprimer/<?= $idCommande ?>" title="Supprimer" onclick="return confirm('Voulez-vous vraiment supprimer cet enregistrement ?')"><span class="col-12 p-0">Supprimer</span></a>
                            </div>
                        </div>
                    </div>
                </article>

                </li>
			    <?php endif; endforeach;?>
            <?php } else { ?>
				<li><h2>Vous n'avez pas encore passé de commande.</h2></li>
			<?php }?>
		</ul>
	</div>
   </section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer_compte.php'); ?>
