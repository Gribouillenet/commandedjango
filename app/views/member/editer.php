<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_compte.php'); ?>
<section class="page-inner clearfix">
	<header class="page-header d-flex flex-column">
  <h1 class="text-xs-center">Fiche client de la radio <?php echo $data['member']['radio_name'];?></h1>
	</header>
          <p>Vous pouvez ici modifier vos informations personnelles. N'oubliez pas d'enregistrer !</p>

          <?php if ( isset( $data['erreur']['name'] ) || isset( $data['erreur']['tel_fixe'] ) || isset( $data['erreur']['tel_mob'] ) || isset( $data['erreur']['mail_member'] ) || isset( $data['erreur']['mail_delivery_member'] ) ) { ?>
            <div class="alert alert-danger alert-on"><?= $data['erreur']['champ_obligatoire'] ?></div>
          <?php } else { ?>
            <span class="champ-obligatoire d-inline-block">* Champs Obligatoires</span>
          <?php } ?>

          <form name="form1" id="form1" class="form-label form-fiche-client" action="" method="post" class="p-y-3 p-x-2" enctype="multipart/form-data" novalidate onkeypress="refuserToucheEntree(event)">
			<label for="name">Nom et prénom du commercial*</label>
			<input type="text" name="name" class="form-control form-obligatoire" placeholder="Nom et prénom du commercial" value="<?php echo $data['member']['name']; ?>">
			<?php if ( isset( $data['erreur']['name'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['name'] ?></div>
            <?php endif; ?>

			<label for="name">Téléphone fixe*</label>
 			<input type="text" name="tel_fixe" class="form-control form-obligatoire" placeholder="Téléphone fixe" value="<?php echo $data['member']['tel_fixe']; ?>">
            <?php if ( isset( $data['erreur']['tel_fixe'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['tel_fixe'] ?></div>
            <?php endif; ?>

            <label for="name">Téléphone mobile*</label>
            <input type="text" name="tel_mob" class="form-control form-obligatoire" placeholder="Téléphone mobile" value="<?php echo $data['member']['tel_mob']; ?>">
            <?php if ( isset( $data['erreur']['tel_mob'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['tel_mob'] ?></div>
            <?php endif; ?>

			<label for="mail_member">Email du commercial*</label>
			<input type="text" name="mail_member" class="form-control form-obligatoire" placeholder="Email du commercial" value="<?php echo $data['member']['mail_member']; ?>">
			<?php if ( isset( $data['erreur']['mail_member'] ) ) : ?>
                <div class="alert alert-danger alert-form"><?= $data['erreur']['mail_member'] ?></div>
            <?php endif; ?>

			<label for="mail_delivery_member">Email de livraison*</label>
			<input type="text" name="mail_delivery_member" class="form-control form-obligatoire" placeholder="Email de livraison" value="<?php echo $data['member']['mail_delivery_member'];?>">
            <?php if ( isset( $data['erreur']['mail_delivery_member'] ) ) : ?>
                <div class="alert alert-danger"><?= $data['erreur']['mail_delivery_member'] ?></div>
            <?php endif; ?>

			<input type="submit" class="btn btn-record" value="Enregistrer">
          </form>
      <a href="/member/compte" class="link-come-back"><button>Retour</button></a>
</section>
<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
