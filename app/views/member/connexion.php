<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_home.php');?>
	<section class="page-inner clearfix">
		<header class="page-header">
			<h1 class="text-xs-center">Se connecter</h1>
		</header>
		  <?php if ( isset( $data['erreur'] ) ) : ?>
		    <div class="alert alert-danger alert-on"><?= $data['erreur'] ?></div>
		  <?php endif; ?>
		  	<form name="form1" id="form1" class="connexion-inner" action="/member/connexion" method="post" class="p-y-3 p-x-2" novalidate onkeypress="refuserToucheEntree(event)">
		    	<input type="text" name="login" class="form-control" placeholder="Identifiant" value="<?php if ( isset( $_POST['login'] ) ) echo $_POST['login'] ?>">
				<input type="password" name="password" class="form-control" placeholder="Mot de passe">
		    <div class="btn btn-connexion">
			    <input type="submit" value="Connexion">
		    </div>
			</form>
	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php'); ?>
