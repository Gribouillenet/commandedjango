<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/header_formules.php');?>
		<section class="page-inner">
		<header class="page-header d-flex flex-column">
	   		<h1 class="text-xs-center">Faites le choix d'une formule</h1>
	   		<?php $membre = $data['membre'];

		   		if($membre) :
	   		?>
	   		<aside class="infos-membre">


		   		<h2><?= $membre['radio_name'] ?></h2>
		   		<h3><?= $membre['name']?></h3>
		   		<ul>
			   		<li><?= $membre['tel_mob']?></li>
			   		<li><?= $membre['tel_fixe']?></li>
			   		<li><?= $membre['mail_member']?></li>
		   		</ul>
	   		</aside>
	   		<?php endif;?>
		</header>
		<div class="row">
			<?php

			foreach( $data['formules'] as $key => $formule ) :

		        $title = $formule['title'];
		        $title_link = str_replace( ' ', '_', $title );
				$title_link = htmlentities($title_link, ENT_NOQUOTES, 'UTF-8');
		        $title_link = preg_replace('#&([A-za-z])(?:uml|circ|tilde|acute|grave|cedil|ring);#', '\1', $title_link);
				$title_link = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $title_link);
				$title_link = preg_replace('#&[^;]+;#', '', $title_link);
		        $title_link = strtolower( $title_link );
		        $body = $formule['body'];
		        $id = $formule['id'];
				$name =  $membre['name'];
			?>
	        <div class="col-md-4">
		        <a class="link-formule" href="/formules/formule_<?= $title_link ?>" title="choisir la formule : <?= $title ?>">
					<article class="formule-container">
						<header class="header-formule-<?= $id?> d-flex flex-column justify-content-between">
							<h1><?= $title ?></h1>
							<aside class="d-flex aside-formule-<?= $id?>">
								<div class="btn-link-formule">
									<span>Choisir cette formule : <?= $title ?></span>
								</div>
							</aside>
						</header>
		            	<p><?= $body ?></p>
					</article>
				</a>
	        </div>
			<?php endforeach;?>
    	</div>
	</section>

<?php require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');?>