<?php include('inc/utf8_errors.php'); ?>
    	<title>Choix de votre formule | Commandes - Django studio</title>
    </head>
<body>
	  <header id="masthead" class="site-header" role="banner">
		<div id="site-header-sticky" class="site-header-sticky clearfix">
			<div class="site-inner d-flex justify-content-center justify-content-md-end align-items-center">
				<p class="widget-tel">01 48 38 34 19</p>
				<nav id="sticky-navigation" class="sticky-navigation" role="navigation">
					<ul id="menu-menu-top" class="sticky-menu">
						<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158 contactez-nous">
							<a title="Nous contacter" href="http://django.fr/contactez-nous/">
								<span class="sticky-link">Contactez-nous</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<nav id="primary-navigation" class="primary-navigation">
			<div class="site-inner d-flex align-items-center flex-column justify-content-md-between flex-md-row">
				<div class="site-header-branding clearfix">
					<div class="site-branding">
						<a class="site-logo" href="/formules" class="navbar-brand" rel="home">Commandes - Django Studio</a>
					</div>
				</div>
		        <ul class="nav navbar d-flex">
					<li class="nav-item"><a class="nav-link nav-link-compte" href="/member/compte">Mon compte</a></li>
					<li class="nav-item nav-item-active"><a class="nav-link nav-link-choix-formules" href="/formules">Choix de formules</a></li>
					<li class="nav-item"><a class="nav-link nav-link-deconnexion" href="/member/deconnexion">D&eacute;connexion</a></li>
		        </ul>
		      </div>
	    </nav>
	</header>
	<div class="site-inner clearfix">