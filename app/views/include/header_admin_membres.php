<?php include('inc/utf8_errors.php'); ?>
	    <title>Admin | Commandes by Django</title>
	</head>
<body>
	<header id="masthead" class="site-header" role="banner">
		<nav id="primary-navigation" class="primary-navigation">
			<div class="site-inner d-flex align-items-center flex-column justify-content-md-between flex-md-row">
				<div class="site-header-branding clearfix">
					<div class="site-branding">
						<a class="site-logo" href="/admin" class="navbar-brand" rel="home">Commandes - Django Studio</a>
					</div>
				</div>
		        <ul class="nav navbar d-flex">
		          <li class="nav-item"><a class="nav-link nav-link-gestion-formules" href="/admin">G&eacute;rer les formules</a></li>
				  <li class="nav-item nav-item-active"><a class="nav-link nav-link-gestion-membres" href="/admin/membres">G&eacute;rer les clients</a></li>
		          <li class="nav-item"><a class="nav-link nav-link-deconnexion" href="/admin/deconnexion">D&eacute;connexion</a></li>
		        </ul>
		      </div>
	    </nav>
	</header>
	<div class="site-inner">