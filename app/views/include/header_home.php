<?php
	include('inc/utf8_errors.php');
?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	    <title>Django studio - commandes</title>
    </head>
<body>
	<header id="masthead" class="site-header" role="banner">
		<div id="site-header-sticky" class="site-header-sticky clearfix">
			<div class="site-inner d-flex justify-content-center justify-content-md-end align-items-center">
				<p class="widget-tel">01 48 38 34 19</p>
				<nav id="sticky-navigation" class="sticky-navigation" role="navigation">
					<ul id="menu-menu-top" class="sticky-menu">
						<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158 contactez-nous">
							<a title="Nous contacter" href="http://django.fr/contactez-nous/">
								<span class="sticky-link">Contactez-nous</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<nav id="primary-navigation" class="primary-navigation">
			<div class="site-inner d-flex align-items-center flex-column justify-content-md-between flex-md-row">
				<?php include('inc/branding.html');?>
			</div>
	    </nav>
	</header>
    <div class="site-inner">
