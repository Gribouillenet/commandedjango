<?php
		ini_set('display_errors',1);
		ini_set('default_charset','UTF-8');
		header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex,nofollow"/>
	<meta name="msapplication-TileImage" content="/img/jpg/django-favicon.jpg" />
	<link rel="icon" href="/img/jpg/django-favicon.jpg" sizes="32x32" />
	<link rel="icon" href="/img/jpg/django-favicon.jpg" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="/img/jpg/django-favicon.jpg" />
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">

