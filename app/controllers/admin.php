<?php
class Admin extends Controller {

// AFFICHAGE DES FORMULES
	public function index() {

		if( !isset( $_SESSION['id'] ) ) {

			header( 'Location: /admin/connexion' );
		}

		$formules = DB::select( 'select * from formules order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );

			$erreur = [];

            if ( empty ( $title ) || empty( $body ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $body ) ) {
				$erreur['body'] = 'Texte obligatoire !';
			}
			if( !$erreur) {
				DB::insert( 'insert into formules (title, body) values (:title, :body)', [
					'title' => htmlspecialchars( $title ),
					'body' => htmlspecialchars( $body )
				] );

				header( 'Location: /admin' );
			}
			else{
				$erreur[''] = 'Tous les champs doivent être remplis';
			}

			$this->view( 'admin/formules', ['erreur' => $erreur, 'formules' => $formules] );
		}

		$this->view( 'admin/formules', ['formules' => $formules] );

	}

//PUBLIER UNE FORMULE
	public function formules() {

		if( !isset( $_SESSION['id'] ) ) {

			header( 'Location: /admin/connexion' );
		}

		$formules = DB::select( 'select * from formules order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );

			$erreur = [];

            if ( empty ( $title ) || empty( $body ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $body ) ) {
				$erreur['body'] = 'Texte obligatoire !';
			}
			if( !$erreur) {
				DB::insert( 'insert into formules (title, body) values (:title, :body)', [
					'title' => htmlspecialchars( $title ),
					'body' => htmlspecialchars( $body )
				] );

				header( 'Location: /admin' );
			}
			else{
				$erreur[''] = 'Tous les champs doivent être remplis';
			}

			$this->view( 'admin/index', ['erreur' => $erreur, 'formules' => $formules] );
		}

		$this->view( 'admin/index', ['formules' => $formules] );

	}

//SUPPRIMER UNE FORMULE
	public function supprimer( int $id ) {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

		$formules = DB::delete ( 'delete from formules where id = ?', [$id] );

		header( 'Location: /admin' );
	}

//EDITER UNE FORMULE
	public function editer( int $id ) {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

		$formules = DB::select( 'select * from formules where id = ?', [$id] );


		if  ( !$formules ){
			header( 'Location: /admin' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

            if ( empty ( $title ) || empty( $body ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $title ) ) {
				$erreur['title'] = 'Titre obligatoire !';
			}

			if( empty ( $body ) ) {
				$erreur['body'] = 'Texte obligatoire !';
			}
			if( !$erreur) {
				DB::update( 'update formules set title = :title, body = :body where id = :id', [
					'title' => htmlspecialchars( $title ),
					'body' => htmlspecialchars( $body ),
					'id' => $id
				] );

				header( 'Location: /admin/' . $id );
			}
			else{
                $erreur[''] = 'Tous les champs doivent être remplis';
			}

            $this->view( 'admin/editer', ['erreur' => $erreur, 'formules' => $formules[0]] );
		}

		$this->view( 'admin/editer', ['formules' => $formules[0]] );

	}

//CRÉATION D'UN MEMBRE
	public function membres() {

		if( !isset( $_SESSION['id'] ) ) {

			header( 'Location: /admin/connexion' );
		}

		$member = DB::select( 'select * from member order by id asc' );

		if( !empty( $_POST ) ) {
			extract( $_POST );

			$erreur = [];

            if ( empty ( $radio_name ) || empty ( $name ) || empty ( $tel_fixe ) || empty ( $tel_mob ) || empty ( $mail_member ) || empty ( $mail_delivery_member ) || empty ( $login ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $radio_name ) ) {
				$erreur['radio_name'] = 'Nom de la radio obligatoire !';
			}
            if( empty ( $name ) ) {
				$erreur['name'] = 'Nom et prénom du commercial obligatoire !';
			}

            function validerNumero($telATester) {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if( empty ( $tel_fixe ) ) {
				$erreur['tel_fixe'] = 'Téléphone fixe obligatoire !';
			}

			if( empty ( $tel_mob ) ) {
				$erreur['tel_mob'] = 'Téléphone mobile obligatoire !';
			}

            if( !empty ( $tel_fixe ) ) {
                if ( !filter_var( $tel_fixe, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_fixe'] = 'Merci de renseigner un téléphone valide.';
                }
			}

            if( !empty ( $tel_mob ) ) {
                if ( !filter_var( $tel_mob, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_mob'] = 'Merci de renseigner un téléphone valide.';
                }
			}

			if( empty ( $mail_member ) ) {
				$erreur['mail_member'] = 'Email obligatoire !';
			}
			elseif ( !filter_var( $mail_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_member'] = 'L\'adresse email est invalide';
			}
            if( empty ( $mail_delivery_member ) ) {
				$erreur['mail_delivery_member'] = 'Email obligatoire !';
			}
			elseif ( !filter_var( $mail_delivery_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_delivery_member'] = 'L\'adresse email est invalide';
			}
            if( empty ( $login ) ) {
				$erreur['login'] = 'Identifiant obligatoire !';
			}

			$since = date("Y-m-d H:m:s");
			$password1 = $this->generatePassword();
			$password = md5($password1);
			//$password = password_hash($password, PASSWORD_DEFAULT );

			if( !$erreur) {

				DB::insert( 'insert into member (radio_name, name, tel_fixe, tel_mob, mail_member, mail_delivery_member, login, password) values
				(:radio_name, :name, :tel_fixe, :tel_mob, :mail_member, :mail_delivery_member, :login, :password)', [
					'radio_name' => htmlspecialchars( $radio_name ),
					'name' => htmlspecialchars( $name ),
					'tel_fixe' => htmlspecialchars( $tel_fixe ),
					'tel_mob' => htmlspecialchars( $tel_mob ),
					'mail_member' => htmlspecialchars( $mail_member ),
					'mail_delivery_member' => htmlspecialchars( $mail_delivery_member ),
					'login' => htmlspecialchars( $login ),
					'password' =>   ($password)
					] );
					$to = $mail_member;
			$subject = 'Django Studio - Commandes | Vos identifiants';

			$member_last = DB::select( 'SELECT * FROM member WHERE member.id = (SELECT MAX(id) FROM member)' );

			$message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Django Studio - Vos identifiants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #F2F0F0; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
    p{margin: 15px 0;}

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;" style="background-color:#F2F0F0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#F2F0F0;">
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
			                    <td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
	                                                <p style="margin: 15px 0;">Bonjour,<br><br>Afin de faciliter vos demandes, Django Studio a mis en place pour vous une nouvelle plateforme de commande.</p>
	                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre mot de passe pour vous connecter à cet utilitaire. Ne perdez plus de temps découvrez-le dès maintenant !</p>
	                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
		                                                <a href="https://commandes.django.fr" title="Découvrir l\'utilitaire de commandes Django Studio" target="_blank" style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Commandes - Django Studio</button>
			                                            </a>
			                                        </p>
			                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
			                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $member_last[0]['login'] .'</b></p>
			                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password1 . '<b></p>
			                                        <p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>
			                                    </td>
                                                <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
</body>
</html>';
			$headers = 'From: production@django.fr' . "\r\n" .	'Reply-To: no-reply@django.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
			'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
 				header( 'Location: /admin/membres' );
			}
			else{
			//echo "<pre>";
			//print_r($erreur);exit;
				$erreur[''] .= 'Tous les champs doivent être remplis !';
			}

			$this->view( 'admin/member', ['erreur' => $erreur, 'member' => $member] );
		}

		$this->view( 'admin/member', ['member' => $member] );

	}

//SUPPRIMER UN MEMBRE
	public function supprimer_client( int $id ) {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

		$member = DB::delete ( 'delete from member where id = ?', [$id] );

		header( 'Location: /admin/membres' );
	}

//EDITER UN MEMBRE
	public function editer_client( int $id ) {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

		$member = DB::select( 'select * from member where id = ?', [$id] );


		if  ( !$member ){
			header( 'Location: /admin/membres' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

			if ( empty ( $radio_name ) || empty ( $name ) || empty ( $tel_fixe ) || empty ( $tel_mob ) || empty ( $mail_member ) || empty ( $mail_delivery_member ) || empty ( $login ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $radio_name ) ) {
				$erreur['radio_name'] = 'Nom de la radio obligatoire !';
			}
            if( empty ( $name ) ) {
				$erreur['name'] = 'Nom et prénom du commercial obligatoire !';
			}

			function validerNumero($telATester) {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if( empty ( $tel_fixe ) ) {
				$erreur['tel_fixe'] = 'Téléphone fixe obligatoire !';
			}

			if( empty ( $tel_mob ) ) {
				$erreur['tel_mob'] = 'Téléphone mobile obligatoire !';
			}

            if( !empty ( $tel_fixe ) ) {
                if ( !filter_var( $tel_fixe, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_fixe'] = 'Merci de renseigner un téléphone valide.';
                }
			}

            if( !empty ( $tel_mob ) ) {
                if ( !filter_var( $tel_mob, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_mob'] = 'Merci de renseigner un téléphone valide.';
                }
			}

			if( empty ( $mail_member ) ) {
				$erreur['mail_member'] = 'Email obligatoire !';
			}
			elseif ( !filter_var( $mail_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_member'] = 'L\'adresse email est invalide';
			}
            if( empty ( $mail_delivery_member ) ) {
				$erreur['mail_delivery_member'] = 'Email obligatoire !';
			}
			elseif ( !filter_var( $mail_delivery_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_delivery_member'] = 'L\'adresse email est invalide';
			}
            if( empty ( $login ) ) {
				$erreur['login'] = 'Identifiant obligatoire !';
			}

			$since=date("Y-m-d H:m:s");

			if( !$erreur) {
			//echo'update  member set radio_name = '.$radio_name.', name ='.$name.', tel_fixe ='.$tel_fixe.', tel_mob ='.$tel_mob.', mail_member ='.$mail_member.',
			//	login ='.$login.' where id = '.$id.'' ;

				DB::update( 'update member set radio_name = :radio_name, name = :name, tel_fixe = :tel_fixe, tel_mob = :tel_mob, mail_member = :mail_member, mail_delivery_member = :mail_delivery_member, login = :login where id = :id' , [
					'radio_name' => htmlspecialchars( $radio_name ),
					'name' => htmlspecialchars( $name ),
					'tel_fixe' => htmlspecialchars( $tel_fixe ),
					'tel_mob' => htmlspecialchars( $tel_mob ),
					'mail_member' => htmlspecialchars( $mail_member ),
					'mail_delivery_member' => htmlspecialchars( $mail_delivery_member ),
					'login' => htmlspecialchars( $login ),
					'id' => $id
				] );
 				header( 'Location: /admin/membres' );
			}

			else{
				$this->view( 'admin/editer_client', ['erreur' => $erreur, 'member' => $member[0]] );
			}
		}

		$this->view( 'admin/editer_client', ['member' => $member[0]] );

	}

//HISTORIQUE D'UN MEMBRE
    public function historique_client( int $id ) {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

        $commandes = DB::select( 'SELECT * FROM commandes, member, formules WHERE member.id = ? AND ref_Member = member.id AND
		ref_Formules = formules.id GROUP BY commandes.id ORDER BY commandes.id DESC', [$id] );

        $membre = DB::select( 'SELECT * FROM  member WHERE  member.id = ?', [$id] );

		if(!$commandes){
		  $commandes = DB::select( 'SELECT * FROM  commandes WHERE ref_Member.id = ?  ', [$id] );
		}

        foreach ( $commandes as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$commandes[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );

				$dateCommande = date_create( $commande['date_commande'] );
				$commandes[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );

				$commandes[$key]['observations'] = nl2br( $commande['observations'] );
				$commandes[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
			}

        $this->view( 'admin/historique_client', ['account_commandes' => $commandes, 'account_membre' => $membre] );
    }

//SUPPRIMER UNE FORMULE
    public function supprimer_hist( int $idCommande ) {
            if( !isset( $_SESSION['id'] ) ) {
                header( 'Location: /admin/historique_client' );
            }

            $membre = DB::select( 'SELECT * FROM commandes WHERE id = ?', [$idCommande] );

            $commandes = DB::delete ( 'delete from commandes where id = ?', [$idCommande] );

            header( 'Location: /admin/historique_client/'.$membre[0]['ref_Member'] );
        }

//GÉNÉRÉ UN MOT MOT DE PASSE
    private function generatePassword ($length = 8){
		$genpassword = "";
		$possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;

		while ($i < $length) {
	    	$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
				if (!strstr($genpassword, $char)) {
					$genpassword .= $char;
					$i++;
	    		}
		}
		return $genpassword;
	}

//RE-ÉDITER UN MOT DE PASSE
	public function password( int $id ) {

		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}


		$member = DB::select( 'select * from member where id = ?', [$id] );

		if  ( !$member ){
			header( 'Location: /admin/membres' );
		}
			$mail_member = $member[0]['mail_member'];
			$password2 = $this->generatePassword();
			$password = md5($password2);
			//$password = password_hash($password2, PASSWORD_DEFAULT );

			DB::update( 'update  member set password = :password  where id = :id' , [
								'password' =>  $password ,
								'id' => $id
							] );
			$to      = $mail_member;
			$subject = 'Django Studio - Commandes | Renouvellement de vos identifiants';
			$message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Django Studio - Vos identifiants</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #F2F0F0; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#000000; font-size:22px; }
    p{margin: 15px 0;}

    @media only screen and (max-width: 480px) {

        table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
        table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
        table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
        table[class=w640], td[class=w640], img[class=w640] { width:300px !important; }
        img{ height:auto;}
         /*illisible, on passe donc sur 3 lignes */
        table[class=w180], td[class=w180], img[class=w180] {
            width:280px !important;
            display:block;
        }
        td[class=w20]{ display:none; }
    }

    </style>

</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;" style="background-color:#F2F0F0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#F2F0F0;">
        <tbody>
            <tr>
                <td align="center" bgcolor="#581E62">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>

                            <tr class="pagetoplogo">
	                            <td class="w640" width="640">
		                            <table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#F2F0F0">
			                            <tbody>
				                            <td class="w30" width="30">
					                            <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-left-email.jpg" alt="bg-left-email" width="" height="" />
				                            </td>
				                            <td class="w580" width="580">
					                             <img class="w580" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-top-email.jpg" alt="bg-top-email" width="" height="" />
				                            </td>
				                            <td class="w30" width="30">
					                             <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/jpg/bg-right-email.jpg" alt="bg-right-email" width="" height="" />
				                            </td>
			                            </tbody>
		                            </table>
	                            </td>
	                        <tr>
		                    <tr class="content">
			                    <td class="w640" width="640" bgcolor="#FFFFFF">
			                     	<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="w30" width="30">
	                                                <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                </td>
                                                <td class="w580" width="580">
	                                                <p style="margin: 15px 0;">Bonjour,<br><br>Vous nous avez demander de vous générer un nouveau mot de passe.</p>
	                                                <p style="margin: 15px 0;">Veuillez trouver ci-après votre identifiant ainsi que votre nouveau mot de passe pour vous connecter à l\'utilitaire de commandes</p>
	                                                <p style="text-align:center; font-size: 14px; margin: 15px 0">
		                                                <a href="https://commandes.django.fr" title="Découvrir l\'utilitaire de commandes Django Studio" target="_blank" style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;"><button style="background-color: #841A59; border:none; color: #FFFFFF; cursor: pointer; font-size:18px; box-shadow: none!important; font-weight: bold; padding: 5px;">Commandes - Django Studio</button>
			                                            </a>
			                                        </p>
			                                        <p style="margin: 15px 0;">Nous restons bien évidemment à votre disposition pour vous accompagner.</p><br>
			                                        <p style="margin: 0 0 15px;">Votre identifiant : <b>' . $member[0]['login'] . '</b></p>
			                                        <p style="margin: 15px 0;">Votre mot de passe : <b>' . $password2 . '<b></p>
													<p style="margin: 15px 0">Merci de votre confiance.<br>L\'équipe de Django Studio</p>
			                                        <p style="margin: 15px 0;">
				                                        <em style="color: #938795; font-size: 10px; font-weight: normal;">
				                                        	Django Studio© 2018. Tous droits réservés. Application réalisée par <a href="https://gribouillenet.fr" target="_blank" title="Voir les autres réalisations du webmaster">Gribouillenet©</a>
				                                        </em>
				                                    </p>
			                                    </td>
                                                <td class="w30" width="30">
	                                                 <img class="w30" style="text-decoration: none; display: block;" src="https://commandes.django.fr/img/gif/white-space.gif" alt="space" width="" height="" />
                                                 </td>
                                            </tr>
                                        </tbody>
			                     	</table>
		                     	</td>
		                    </tr>
		                    <tr>
                                <td class="w640"  width="640" height="20"></td>
                            </tr>
			            </tbody>
			        </table>
			    </td>
			</tr>
		</tbody>
	</table>
</body>
</html>' ;
			$headers = 'From: production@django.fr' . "\r\n" .	'Reply-To: no-reply@django.fr' . "\r\n" . 'Content-Type: text/html; charset=utf-8' .
			'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);

				header( 'Location: /admin/membres' );
	}

//VERIFICATION SI COMPTE EXISTE
	private function accountExists() : array {

		$admin = DB::select( 'select id, password from admin where login = ?', [$_POST['login']] );

		if ( $admin && password_verify( $_POST['password'], $admin[0]['password'] ) ) {
			return $admin[0];
		}
		else {
			return [];
		}
	}

//CONNEXION
	public function connexion() {

	    if ( !empty( $_POST ) ) {
	      extract( $_POST );

	      $admin = $this->accountExists();

			if ( $admin ) {
				$_SESSION['id'] = $admin['id'];

				header( 'Location: /admin' );
			}
			else {
				$erreur = 'Identifiants erronés';
			}

			$this->view( 'admin/connexion', ['erreur' => $erreur] );
	    }

		$this->view( 'admin/connexion' );
	}

//DECONNEXION
	public function deconnexion(){
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /admin/connexion' );
		}

		$_SESSION = [];

		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		session_destroy();

		header( 'Location: /admin/connexion' );
	}
}
