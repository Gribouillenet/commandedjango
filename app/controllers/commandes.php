<?php
	class Commandes extends Controller{
// 		COMMANDE EN COURS
		public function index(){
			if( !isset( $_SESSION['id'] ) ) {
				header( 'Location: /member/connexion' );
			}
			$id_membre = $_SESSION['id'];
			$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);
			$commandes = DB::select( 'SELECT * FROM commandes, member, formules WHERE
			commandes.id = (SELECT MAX(id) FROM commandes) AND ref_Member = ? AND ref_Formules = formules.id and member.id = ? group by commandes.id', [$id_membre,$id_membre] );
			foreach ( $commandes as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$commandes[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );
				$dateCommande = date_create( $commande['date_commande'] );
				$commandes[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );
				$commandes[$key]['observations'] = nl2br( $commande['observations'] );
				$commandes[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
			}
			$this->view( 'commandes/index', ['commandes' => $commandes, 'membre' => $membre[0]] );
		}
// 		VALIDATION DE LA COMMANDE
		public function validation(){
			// SUJET
			$id_membre = $_SESSION['id'];
			$commande =  DB::select( 'SELECT * FROM commandes, member, formules WHERE
			commandes.id = (SELECT MAX(id) FROM commandes) AND ref_Member = ? AND ref_Formules = formules.id and member.id = ?', [$id_membre,$id_membre] );
		foreach ( $commande as $key => $validation ){
				$created_at = date_create( $validation['date_commande'] );
				$created_at = date_format( $created_at, 'dmy' );
				$mail = $validation['mail_annonceur'];
				$fichier = $validation['fichier'];
				$fichier_2 = $validation['fichier_2'];
				$fichier_3 = $validation['fichier_3'];
				$idCommande = $validation[0];
				$ref_member = $validation['ref_Member'];
				$radio_name = $validation['radio_name'];
				$commercial_name = $validation['name'];
				$commercial_telFix = $validation['tel_fixe'];
				$commercial_telMob = $validation['tel_mob'];
				$commercial_mail = $validation['mail_member'];
				$mail_livraison =  $validation['mail_livraison']   ;
				if( $idCommande <= 9 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-00000' .$idCommande;
				}
				elseif( $idCommande <= 99 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-0000' . $idCommande;
				}
				elseif( $idCommande <= 999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-000' . $idCommande;
				}
				elseif( $idCommande <= 9999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-00' . $idCommande;
				}
				elseif( $idCommande <= 99999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-0' . $idCommande;
				}
				elseif( $idCommande <= 999999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-' . $idCommande;
				}
				$subject = 'Nouvelle commande : ' . $ref_commande . ' - ' . $validation['nom_campagne'];
// 				var_dump($validation);
			}

<<<<<<< HEAD
			$mail_admin = 'testappligrib@gmail.com';
=======
			$mail_admin = 'production@django.fr';
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			$message = $_POST['commande'];

// 			Attention ne pas oublier de remplacer la valeur de $mail_admin production@django.fr une fois mis en ligne
			$to = ''. $commercial_mail .','. $mail_admin .'';
			$eol="\r\n";
			$headers = 'From: Django studio - Commandes <no-reply@django.fr>' . $eol;
			$headers .= 'Reply-To: no-reply@django.fr' . $eol;
			$headers .= 'Return-Path: no-reply@django.fr' . $eol;
			$headers .= 'MIME-Version: 1.0' . $eol;
			$headers .= 'Content-Type: text/html; charset=utf-8' . $eol;

// 			var_dump($to);
			//FONCTION ENVOI EMAIL
			if( mail( $to,$subject,$message,$headers ) ){
				$this->view( 'commandes/validation', ['validation' => $validation] );
				if( $idCommande <= 9 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-00000' . $idCommande;
				}
				elseif( $idCommande <= 99 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-0000' . $idCommande;
				}
				elseif( $idCommande <= 999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-000' . $idCommande;
				}
				elseif( $idCommande <= 9999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-00' . $idCommande;
				}
				elseif( $idCommande <= 99999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-0' . $idCommande;
				}
				elseif( $idCommande <= 999999 ){
					$ref_commande = 'RÉF. Djo-' . $ref_member . '-' . $created_at . '-' . $idCommande;
				}
				echo '<section class="page-inner"><header class="page-header d-flex flex-column"><h1>Merci</h1><aside class="infos-membre">
				<h2>' . $radio_name .'</h2><h3>' . $commercial_name . '</h3><ul>';
// 				var_dump($to);
				if($commercial_telFix){
					echo '<li>' . $commercial_telFix . '</li>';
				}
				if( $commercial_telMob){
					echo '<li>' . $commercial_telMob . '</li>';
				}
<<<<<<< HEAD
				echo '<li>Email du commercial : ' . $commercial_mail . '</li></ul></p></header><h2>Votre commande va être traitée</h2><p>' . $ref_commande . '</p></section>' ;
=======
				echo '<li>Email du commercial : ' . $commercial_mail . '</li></ul></p></header><h2>Votre commande va être traitée</h2><p>' . $ref_commande . '</p><a href="/formules" class="link-come-back"><button>Retour</button></a></section>' ;
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
			}
			else{
				$this->view( 'commandes/erreur');
				echo '<section class="page-inner">
		<header class="page-header d-flex flex-column"><h1>Votre email n\'a pas pu être envoyé. Merci de nous contacter au besoin.</h1></header></section>';
			require($_SERVER['DOCUMENT_ROOT'].'/app/views/include/footer.php');
			}
		}

// 		MODIFICATION DE COMMANDE EN COURS
		public function modifier(int $id_COMD ){
<<<<<<< HEAD
			if( !isset( $_SESSION['id'] ) ) {
				header( 'Location: /member/connexion' );
			}
			$modifCommande = DB::select( 'SELECT * FROM commandes, member, formules WHERE commandes.id = ? AND ref_Formules = formules.id', [$id_COMD] );
			foreach ( $modifCommande as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$modifCommande[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );
				$dateCommande = date_create( $commande['created_at'] );
				$modifCommande[$key]['created_at'] = date_format( $dateCommande, 'dmy' );
				$modifCommande[$key]['observations'] = nl2br( $commande['observations'] );
				$modifCommande[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
				$modifCommande[$key]['date_livraison_client'] = $commande['date_livraison_client'] ;
			}
			$id_membre = $modifCommande['ref_Member'];
			$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);
// 		var_dump($editCommande);
			if  ( !$modifCommande ){
				header( 'Location: /member' );
			}
			if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];
			if( empty ( $annonceur ) ) {
				$erreur['annonceur'] = 'Merci de renseigner un annonceur et de re-valider votre commande.';
			}
/*
			if( empty ( $nom_campagne ) ) {
				$erreur['nom_campagne'] = 'Champs Nom de la campagne à renseigner';
			}
*/
			if( empty ( $date_livraison_client ) ) {
				$erreur['date_livraison_client'] = 'Merci de renseigner la date de livraison et de re-valider votre commande.';
			}
/*
			if( empty ( $mail_livraison ) ) {
				$erreur['mail_livraison'] = 'Champs Email de livraison à renseigner';
			}
			elseif ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_livraison'] = 'L\'adresse email est erronée';
			}
*/
			if ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_livraison'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
			}
/*
			if( empty ( $type_voix ) ) {
				$erreur['type_voix'] = 'Champs Type de voix à renseigner';
			}
			if( empty ( $interpretation ) ) {
				$erreur['interpretation'] = 'Champs Interprétation à renseigner';
			}
			if( empty ( $duree ) ) {
				$erreur['duree'] = 'Le champs Durée à renseigner';
			}
			if( empty ( $interlocuteur ) ) {
				$erreur['interlocuteur'] = 'Le champs Interlocuteur à renseigner';
			}
*/
/*
			if( empty ( $tel1 ) ) {
				$erreur['tel1'] = 'Le champs Tél. 1 à renseigner';
			}
			if( empty ( $mail ) ) {
				$erreur['mail_annonceur'] = 'Le champs Email à renseigner';
			}
*/
			if ( !filter_var( $mail, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_annonceur'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
			}
			if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document' , 'application/pdf'] ) ){
					$erreur['fichier'] = 'Format de fichier incorrect .doc, .docx, .pdf, .mpeg, .mp4, .mp3, .mov acceptés.';
				}
				elseif ($_FILES['fichier']['size'] > 10485760 ){
					$erreur['fichier'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
			}

			if ( isset( $_FILES['fichier_2'] ) && $_FILES['fichier_2']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier_2']['type'], ['audio/mpeg','audio/mp3' ,'audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
					$erreur['fichier_2'] = 'Format de fichier 2 incorrect .doc,.docx, .pdf, .mpeg, .mp4, .mp3, .mov accept&eacute;s.';
				}
				elseif ($_FILES['fichier_2']['size'] > 10485760 ){
					$erreur['fichier_2'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
			}

			if ( isset( $_FILES['fichier_3'] ) && $_FILES['fichier_3']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier_3']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
					$erreur['fichier_3'] = 'Format de fichier 3 incorrect .doc,.docx, .pdf, .mpeg, .mp4, .mp3, .mov accept&eacute;s.';
				}
				elseif ($_FILES['fichier_3']['size'] > 10485760 ){
					$erreur['fichier_3'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
=======

			if( !isset( $_SESSION['id'] ) ) {
				header( 'Location: /member/connexion' );
			}

			$modifCommande = DB::select( 'SELECT * FROM commandes, member, formules WHERE commandes.id = ? AND ref_Formules = formules.id', [$id_COMD] );

			foreach ( $modifCommande as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$modifCommande[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );
				$dateCommande = date_create( $commande['created_at'] );
                $ref_formules = $commande['ref_Formules'];
                $id_membre = $commande['ref_Member'];
				$modifCommande[$key]['created_at'] = date_format( $dateCommande, 'dmy' );
				$modifCommande[$key]['observations'] = nl2br( $commande['observations'] );
				$modifCommande[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
				$modifCommande[$key]['date_livraison_client'] = $commande['date_livraison_client'];
			}

			$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

			if  ( !$modifCommande ){
				header( 'Location: /member' );
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			}
			if( !$erreur) {
					DB::update('update member set mail_delivery_member =:mail_livraison where id = :id',
			[ 'mail_livraison' => htmlspecialchars( $mail_livraison),'id'=> $id_membre]);

<<<<<<< HEAD
					DB::update( 'update commandes set annonceur = :annonceur, nom_campagne = :nom_campagne, date_livraison = :date_livraison_client, mail_livraison = :mail_livraison, type_voix = :type_voix, interpretation = :interpretation, duree = :duree, style_musique = :style_musique, style_message = :style_message, style_personnalise = :style_personnalise, interlocuteur = :interlocuteur, tel1 = :tel1, tel2 = :tel2, mail_annonceur = :mail_annonceur, observations = :observations, elemts_texte = :elemts_texte where id = :id', [
					'annonceur' => htmlspecialchars( $annonceur ),
					'nom_campagne' => htmlspecialchars( $nom_campagne ),
					'date_livraison_client' => htmlspecialchars( $date_livraison_client ),
					'mail_livraison' => htmlspecialchars( $mail_livraison ),
					'type_voix' => htmlspecialchars( $type_voix ),
					'interpretation' => htmlspecialchars( $interpretation ),
					'duree' => htmlspecialchars( $duree ),
					'style_musique' => htmlspecialchars( $style_musique ),
					'style_message' => htmlspecialchars( $style_message ),
					'style_personnalise' => htmlspecialchars( $style_personnalise ),
					'interlocuteur' => htmlspecialchars( $interlocuteur ) ,
					'tel1' => htmlspecialchars( $tel1 ),
					'tel2' => htmlspecialchars( $tel2 ),
					'mail' => htmlspecialchars( $mail ),
					'observations' => htmlspecialchars( $observations ),
					'elemts_texte' => htmlspecialchars( $elemts_texte ),
					'id' => $id_COMD,
					] );


					//FICHIERS
					$created_at = date_create( $_POST['date_commande'] );
					$created_at = date_format( $created_at, 'dmy' );
					$ref_member = $id_membre;
					$filname = $_FILES['fichier']['name'];
				 	$filename_2 = $_FILES['fichier_2']['name'];
					$filename_3 = $_FILES['fichier_3']['name'];
					$tab = explode('.', $filname);
					$tab_2 = explode('.', $filename_2);
					$tab_3 = explode('.', $filename_3);
					$comm = DB::select ('SELECT max(id) as cmd FROM commandes');
					$com = $comm[0]['cmd'];
					$ext = $tab[count( $tab )-1];

					$ext_2 = $tab_2[count( $tab_2 )-1];
					$ext_3 = $tab_3[count( $tab_3 )-1];
					$camp = $_POST['nom_campagne'];
					$camp = str_replace(' ', '-', $camp);

					$ref_commande = 'cmd-Djo-' . $ref_member . '-' . $created_at;

					if($ext)  $filename = $ref_commande . '-' . $com . '-briefcomplet-1-' . $camp . '.' . $ext;
					if($ext_2)$filename_2 = $ref_commande.'-' . $com. '-briefcomplet-2-' . $camp . '.' . $ext_2;
					if($ext_3) $filename_3 = $ref_commande.'-' . $com . '-briefcomplet-3-' . $camp . '.' . $ext_3;


					move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename );
					DB::update( 'update commandes set fichier = :fichier WHERE id = '. $com , [ 'fichier' => $filename ]);

					move_uploaded_file( $_FILES['fichier_2']['tmp_name'], ROOT . 'public/files/' . $filename_2 );
					DB::update( 'update commandes set fichier_2 = :fichier_2 WHERE id = '. $com , [ 'fichier_2' => $filename_2 ]);

					move_uploaded_file( $_FILES['fichier_3']['tmp_name'], ROOT . 'public/files/' . $filename_3 );
					DB::update( 'update commandes set fichier_3 = :fichier_3 WHERE id = '. $com , [ 'fichier_3' => $filename_3 ]);


					header( 'Location: /commandes/modifier/' . $id_COMD );
				}
				else{
					$this->view( 'commandes/modifier', ['erreur' => $erreur, 'commandes' => $modifCommande[0]] );
				}
=======
			if ( !empty( $_POST ) ) {
                extract( $_POST );
                $erreur = [];

                if( empty ( $annonceur ) ) {
                    $erreur['annonceur'] = 'Merci de renseigner un annonceur et de re-valider votre commande.';
                }

                if( empty ( $date_livraison_client ) ) {
                    $erreur['date_livraison_client'] = 'Merci de renseigner la date de livraison et de re-valider votre commande.';
                }

                if( empty( $mail_livraison )&&(($membre['mail_delivery_member']=="") ||($membre['mail_delivery_member']== null)) ) {
                    $erreur['date_livraison_client'] = 'Merci de renseigner un email de livraison et de re-valider votre commande.';
                }
                if( !empty ( $mail_livraison) ){
                    if ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
                        $erreur['mail_livraison'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
                    }
                }

                if( !empty ( $duree ) ) {
                    if ( !filter_var( $duree, FILTER_SANITIZE_NUMBER_INT ) ) {
                        $erreur['duree'] = 'Merci de renseigner une durée valide.';
                    }
                }

                function validerNumero($telATester) {
                    //Retourne le numéro s'il est valide, sinon false.
                    return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
                }

                if( !empty ( $tel1 ) ) {
                    if ( !filter_var( $tel1, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
                        $erreur['tel1'] = 'Merci de renseigner un téléphone valide.';
                    }
                }

                if( !empty ( $tel2 ) ) {
                    if ( !filter_var( $tel2, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
                        $erreur['tel2'] = 'Merci de renseigner un téléphone valide.';
                    }
                }

                if( !empty ( $mail_annonceur ) ){
                    if ( !filter_var( $mail_annonceur, FILTER_VALIDATE_EMAIL ) ){
                        $erreur['mail_annonceur'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
                    }
                }

                if( $ref_formules == 1 ) {
                    if ( empty ( $annonceur ) || empty ( $date_livraison_client ) || empty ( $interlocuteur ) || empty ( $tel1 ) ) {
                        $erreur['champ_obligatoire'] = '* Champs Obligatoires';
                    }

                    if( empty ( $interlocuteur ) ) {
                        $erreur['interlocuteur'] = 'Merci de renseigner un interlocuteur et de re-valider votre commande.';
                    }

                    if( empty ( $tel1 ) ) {
                        $erreur['tel1'] = 'Merci de renseigner un numéro de téléphone.';
                    }
                }

                if( ($ref_formules == 2) || ($ref_formules == 3) ) {
                    if ( empty ( $annonceur ) || empty ( $date_livraison_client ) || empty ( $elemts_texte ) ) {
                        $erreur['champ_obligatoire'] = '* Champs Obligatoires';
                    }

                    if( empty ( $elemts_texte ) ) {
                        $erreur['elemts_texte'] = 'Merci de renseigner un élément de texte et de re-valider votre commande.';
                    }
                }

                if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document' , 'application/pdf'] ) ){
                        $erreur['fichier'] = 'Format de fichier 1 incorrect.';
                    }
                    elseif ($_FILES['fichier']['size'] > 10485760 ){
                        $erreur['fichier'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if ( isset( $_FILES['fichier_2'] ) && $_FILES['fichier_2']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier_2']['type'], ['audio/mpeg','audio/mp3' ,'audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
                        $erreur['fichier_2'] = 'Format de fichier 2 incorrect.';
                    }
                    elseif ($_FILES['fichier_2']['size'] > 10485760 ){
                        $erreur['fichier_2'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if ( isset( $_FILES['fichier_3'] ) && $_FILES['fichier_3']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier_3']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
                        $erreur['fichier_3'] = 'Format de fichier 3 incorrect.';
                    }
                    elseif ($_FILES['fichier_3']['size'] > 10485760 ){
                        $erreur['fichier_3'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if( !$erreur) {
                    DB::update('update member set mail_delivery_member =:mail_livraison where id = :id',
                    [ 'mail_livraison' => htmlspecialchars( $mail_livraison ),'id'=> $id_membre]);

                    DB::update( 'update commandes set annonceur = :annonceur, date_livraison_client = :date_livraison_client, mail_livraison = :mail_livraison, interlocuteur = :interlocuteur, tel1 = :tel1 where id = :id', [
                        'annonceur' => htmlspecialchars( $annonceur ),
                        'date_livraison_client' => htmlspecialchars( $date_livraison_client ),
                        'mail_livraison' => htmlspecialchars( $mail_livraison ),
                        'interlocuteur' => htmlspecialchars( $interlocuteur ) ,
                        'tel1' => htmlspecialchars( $tel1 ),
                        'id' => $id_COMD,
                    ] );

                    DB::update('update commandes set nom_campagne =:nom_campagne where id = :id',
                               [ 'nom_campagne' => htmlspecialchars( $nom_campagne ),'id'=> $id_COMD]);

                    DB::update('update commandes set type_voix =:type_voix where id = :id',
                               [ 'type_voix' => htmlspecialchars( $type_voix ),'id'=> $id_COMD]);

                    DB::update('update commandes set interpretation =:interpretation where id = :id',
                               [ 'interpretation' => htmlspecialchars( $interpretation ),'id'=> $id_COMD]);

                    DB::update('update commandes set duree =:duree where id = :id',
                               [ 'duree' => htmlspecialchars( $duree ),'id'=> $id_COMD]);

                    DB::update('update commandes set style_musique =:style_musique where id = :id',
                               [ 'style_musique' => htmlspecialchars( $style_musique ),'id'=> $id_COMD]);

                    DB::update('update commandes set style_message =:style_message where id = :id',
                               [ 'style_message' => htmlspecialchars( $style_message ),'id'=> $id_COMD]);

                    DB::update('update commandes set style_personnalise =:style_personnalise where id = :id',
                               [ 'style_personnalise' => htmlspecialchars( $style_personnalise ),'id'=> $id_COMD]);

                    DB::update('update commandes set tel2 =:tel2 where id = :id',
                               [ 'tel2' => htmlspecialchars( $tel2 ),'id'=> $id_COMD]);

                    DB::update('update commandes set mail =:mail where id = :id',
                               [ 'mail' => htmlspecialchars( $mail ),'id'=> $id_COMD]);

                    DB::update('update commandes set observations =:observations where id = :id',
                               [ 'observations' => htmlspecialchars( $observations ),'id'=> $id_COMD]);

                    DB::update('update commandes set elemts_texte =:elemts_texte where id = :id',
                               [ 'elemts_texte' => htmlspecialchars( $elemts_texte ),'id'=> $id_COMD]);

                //FICHIERS
                    $created_at = date_create( $_POST['date_commande'] );
                    $created_at = date_format( $created_at, 'dmy' );
                    $ref_member = $id_membre;
                    $filname = $_FILES['fichier']['name'];
                    $filename_2 = $_FILES['fichier_2']['name'];
                    $filename_3 = $_FILES['fichier_3']['name'];
                    $tab = explode('.', $filname);
                    $tab_2 = explode('.', $filename_2);
                    $tab_3 = explode('.', $filename_3);
                    $comm = DB::select ('SELECT max(id) as cmd FROM commandes');
                    $com = $comm[0]['cmd'];
                    $ext = $tab[count( $tab )-1];

                    $ext_2 = $tab_2[count( $tab_2 )-1];
                    $ext_3 = $tab_3[count( $tab_3 )-1];
                    $camp = $_POST['nom_campagne'];
                    $camp = str_replace(' ', '-', $camp);

                    $ref_commande = 'cmd-Djo-' . $ref_member . '-' . $created_at;

                    if($ext)  $filename = $ref_commande . '-' . $com . '-briefcomplet-1-' . $camp . '.' . $ext;
                    if($ext_2) $filename_2 = $ref_commande.'-' . $com. '-briefcomplet-2-' . $camp . '.' . $ext_2;
                    if($ext_3) $filename_3 = $ref_commande.'-' . $com . '-briefcomplet-3-' . $camp . '.' . $ext_3;


                    move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename );
                    DB::update( 'update commandes set fichier = :fichier WHERE id = '. $com , [ 'fichier' => $filename ]);

                    move_uploaded_file( $_FILES['fichier_2']['tmp_name'], ROOT . 'public/files/' . $filename_2 );
                    DB::update( 'update commandes set fichier_2 = :fichier_2 WHERE id = '. $com , [ 'fichier_2' => $filename_2 ]);

                    move_uploaded_file( $_FILES['fichier_3']['tmp_name'], ROOT . 'public/files/' . $filename_3 );
                    DB::update( 'update commandes set fichier_3 = :fichier_3 WHERE id = '. $com , [ 'fichier_3' => $filename_3 ]);

                    header( 'Location: /commandes' );
                }
                else{

                    $erreur[''] = 'Merci de renseigner les champs notifiés et de re-valider votre commande';
                }

                $this->view( 'commandes/modifier', ['erreur' => $erreur, 'commandes' => $modifCommande[0]] );

>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			}
// 			var_dump($editCommande[0]);
			$this->view( 'commandes/modifier', ['commandes' => $modifCommande[0], 'membre' => $membre[0]] );
		}
		//EDITER POUR RECOMMANDE
		function editer( int $id_COMD ) {

			if( !isset( $_SESSION['id'] ) ) {
				header( 'Location: /member/connexion' );
			}
			$editCommande = DB::select( 'SELECT * FROM commandes, member, formules WHERE commandes.id = ? AND ref_Formules = formules.id', [$id_COMD] );
<<<<<<< HEAD
=======

>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			foreach ( $editCommande as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$editCommande[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );
				$dateCommande = date_create( $commande['created_at'] );
                $ref_formules = $commande['ref_Formules'];
                $id_membre = $commande['ref_Member'];
				$editCommande[$key]['created_at'] = date_format( $dateCommande, 'dmy' );
				$editCommande[$key]['observations'] = nl2br( $commande['observations'] );
				$editCommande[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
			}
<<<<<<< HEAD
			$id_membre = $editCommande['ref_Member'];
			$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);
// 		var_dump($editCommande);
=======

			$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);

>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			if  ( !$editCommande ){
				header( 'Location: /member' );
			}
			if ( !empty( $_POST ) ) {
<<<<<<< HEAD
			extract( $_POST );
			$erreur = [];
			if( empty ( $annonceur ) ) {
				$erreur['annonceur'] = 'Merci de renseigner un annonceur et de re-valider votre commande.';
			}
/*
			if( empty ( $nom_campagne ) ) {
				$erreur['nom_campagne'] = 'Champs Nom de la campagne à renseigner';
			}
*/
			if( empty ( $date_livraison_client ) ) {
				$erreur['date_livraison_client'] = 'Merci de renseigner la date de livraison et de re-valider votre commande.';
			}
/*
			if( empty ( $mail_livraison ) ) {
				$erreur['mail_livraison'] = 'Champs Email de livraison à renseigner';
			}
			elseif ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_livraison'] = 'L\'adresse email est erronée';
			}
*/
			if ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_livraison'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
			}
/*
			if( empty ( $type_voix ) ) {
				$erreur['type_voix'] = 'Champs Type de voix à renseigner';
			}
			if( empty ( $interpretation ) ) {
				$erreur['interpretation'] = 'Champs Interprétation à renseigner';
			}
			if( empty ( $duree ) ) {
				$erreur['duree'] = 'Le champs Durée à renseigner';
			}
			if( empty ( $interlocuteur ) ) {
				$erreur['interlocuteur'] = 'Le champs Interlocuteur à renseigner';
			}
*/
/*
			if( empty ( $tel1 ) ) {
				$erreur['tel1'] = 'Le champs Tél. 1 à renseigner';
			}
			if( empty ( $mail ) ) {
				$erreur['mail_annonceur'] = 'Le champs Email à renseigner';
			}
*/
			if ( !filter_var( $mail, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_annonceur'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
			}
			if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document' , 'application/pdf'] ) ){
					$erreur['fichier'] = 'Format de fichier incorrect .doc, .docx, .pdf, .mpeg, .mp4, .mp3, .mov acceptés.';
				}
				elseif ($_FILES['fichier']['size'] > 10485760 ){
					$erreur['fichier'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
			}

			if ( isset( $_FILES['fichier_2'] ) && $_FILES['fichier_2']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier_2']['type'], ['audio/mpeg','audio/mp3' ,'audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
					$erreur['fichier_2'] = 'Format de fichier 2 incorrect .doc,.docx, .pdf, .mpeg, .mp4, .mp3, .mov accept&eacute;s.';
				}
				elseif ($_FILES['fichier_2']['size'] > 10485760 ){
					$erreur['fichier_2'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
			}

			if ( isset( $_FILES['fichier_3'] ) && $_FILES['fichier_3']['error'] == 0 ){
				if ( !in_array( $_FILES['fichier_3']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
					$erreur['fichier_3'] = 'Format de fichier 3 incorrect .doc,.docx, .pdf, .mpeg, .mp4, .mp3, .mov accept&eacute;s.';
				}
				elseif ($_FILES['fichier_3']['size'] > 10485760 ){
					$erreur['fichier_3'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
				}
			}
			if( !$erreur) {
					DB::update('update member set mail_delivery_member =:mail_livraison where id = :id',
			[ 'mail_livraison' => htmlspecialchars( $mail_livraison),'id'=> $id_membre]);

					DB::update( 'update commandes set annonceur = :annonceur, nom_campagne = :nom_campagne, date_livraison = :date_livraison_client, mail_livraison = :mail_livraison, type_voix = :type_voix, interpretation = :interpretation, duree = :duree, style_musique = :style_musique, style_message = :style_message, style_personnalise = :style_personnalise, interlocuteur = :interlocuteur, tel1 = :tel1, tel2 = :tel2, mail_annonceur = :mail_annonceur, observations = :observations, elemts_texte = :elemts_texte where id = :id', [
=======
                extract( $_POST );
                $erreur = [];

                if( empty ( $annonceur ) ) {
                    $erreur['annonceur'] = 'Merci de renseigner un annonceur et de re-valider votre commande.';
                }

                if( empty ( $date_livraison_client ) ) {
                    $erreur['date_livraison_client'] = 'Merci de renseigner la date de livraison et de re-valider votre commande.';
                }

                if( empty( $mail_livraison )&&(($membre['mail_delivery_member']=="") ||($membre['mail_delivery_member']== null)) ) {
                    $erreur['date_livraison_client'] = 'Merci de renseigner un email de livraison et de re-valider votre commande.';
                }
                if( !empty ( $mail_livraison) ){
                    if ( !filter_var( $mail_livraison, FILTER_VALIDATE_EMAIL ) ){
                        $erreur['mail_livraison'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
                    }
                }

                if( !empty ( $duree ) ) {
                    if ( !filter_var( $duree, FILTER_SANITIZE_NUMBER_INT ) ) {
                        $erreur['duree'] = 'Merci de renseigner une durée valide.';
                    }
                }

                function validerNumero($telATester) {
                    //Retourne le numéro s'il est valide, sinon false.
                    return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
                }

                if( !empty ( $tel1 ) ) {
                    if ( !filter_var( $tel1, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
                        $erreur['tel1'] = 'Merci de renseigner un téléphone valide.';
                    }
                }

                if( !empty ( $tel2 ) ) {
                    if ( !filter_var( $tel2, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
                        $erreur['tel2'] = 'Merci de renseigner un téléphone valide.';
                    }
                }

                if( !empty ( $mail_annonceur ) ){
                    if ( !filter_var( $mail_annonceur, FILTER_VALIDATE_EMAIL ) ){
                        $erreur['mail_annonceur'] = 'Merci de renseigner un email valide et de re-valider votre commande.';
                    }
                }

                if( $ref_formules == 1 ) {
                    if ( empty ( $annonceur ) || empty ( $date_livraison_client ) || empty ( $interlocuteur ) || empty ( $tel1 ) ) {
                        $erreur['champ_obligatoire'] = '* Champs Obligatoires';
                    }

                    if( empty ( $interlocuteur ) ) {
                        $erreur['interlocuteur'] = 'Merci de renseigner un interlocuteur et de re-valider votre commande.';
                    }

                    if( empty ( $tel1 ) ) {
                        $erreur['tel1'] = 'Merci de renseigner un numéro de téléphone.';
                    }
                }

                if( ($ref_formules == 2) || ($ref_formules == 3) ) {
                    if ( empty ( $annonceur ) || empty ( $date_livraison_client ) || empty ( $elemts_texte ) ) {
                        $erreur['champ_obligatoire'] = '* Champs Obligatoires';
                    }

                    if( empty( $interlocuteur ) && ( !empty ( $tel1 ) || !empty ( $tel2 ) || !empty ( $mail_annonceur ) ) ) {
                        $erreur['interlocuteur'] = 'Merci de renseigner un interlocuteur et de re-valider votre commande.';
                    }

                    if( empty ( $elemts_texte ) ) {
                        $erreur['elemts_texte'] = 'Merci de renseigner un élément de texte et de re-valider votre commande.';
                    }
                }

                if ( isset( $_FILES['fichier'] ) && $_FILES['fichier']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document' , 'application/pdf'] ) ){
                        $erreur['fichier'] = 'Format de fichier 1 incorrect.';
                    }
                    elseif ($_FILES['fichier']['size'] > 10485760 ){
                        $erreur['fichier'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if ( isset( $_FILES['fichier_2'] ) && $_FILES['fichier_2']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier_2']['type'], ['audio/mpeg','audio/mp3' ,'audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
                        $erreur['fichier_2'] = 'Format de fichier 2 incorrect.';
                    }
                    elseif ($_FILES['fichier_2']['size'] > 10485760 ){
                        $erreur['fichier_2'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if ( isset( $_FILES['fichier_3'] ) && $_FILES['fichier_3']['error'] == 0 ){
                    if ( !in_array( $_FILES['fichier_3']['type'], ['audio/mpeg', 'audio/mp3','audio/mpeg3','audio/mp3' ,'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'audio/x-mpeg-3', 'video/mpeg', 'video/mp4', 'video/quicktime', 'application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'] ) ){
                        $erreur['fichier_3'] = 'Format de fichier 3 incorrect.';
                    }
                    elseif ($_FILES['fichier_3']['size'] > 10485760 ){
                        $erreur['fichier_3'] = 'Fichier trop volumineux (sup&eacute;rieur à 10Mo)';
                    }
                }

                if( !$erreur) {
                    DB::update('update member set mail_delivery_member =:mail_livraison where id = :id',
                    [ 'mail_livraison' => htmlspecialchars( $mail_livraison ),'id'=> $id_membre]);

                    DB::insert( 'insert into commandes (ref_Member, ref_Formules, annonceur, nom_campagne, date_livraison_client, mail_livraison,  type_voix, interpretation, duree, style_musique, style_message, interlocuteur, tel1, tel2, mail_annonceur, observations, elemts_texte) values (:ref_Member, :ref_Formules, :annonceur, :nom_campagne, :date_livraison_client, :mail_livraison,  :type_voix, :interpretation, :duree, :style_musique, :style_message, :interlocuteur, :tel1, :tel2, :mail_annonceur, :observations, :elemts_texte)', [
					'ref_Member' =>$id_membre,
					'ref_Formules' =>$ref_formules,
>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
					'annonceur' => htmlspecialchars( $annonceur ),
					'nom_campagne' => htmlspecialchars( $nom_campagne ),
					'date_livraison_client' => htmlspecialchars( $date_livraison_client ),
					'mail_livraison' => htmlspecialchars( $mail_livraison),

					'type_voix' => htmlspecialchars( $type_voix ),
					'interpretation' => htmlspecialchars( $interpretation ),
					'duree' => htmlspecialchars( $duree ),
					'style_musique' => htmlspecialchars( $style_musique ),
					'style_message' => htmlspecialchars( $style_message ),
					'interlocuteur' => htmlspecialchars( $interlocuteur ) ,
					'tel1' => htmlspecialchars( $tel1 ),
					'tel2' => htmlspecialchars( $tel2 ),
					'mail_annonceur' => htmlspecialchars( $mail_annonceur ),
					'observations' => htmlspecialchars( $observations ),
					'elemts_texte' => htmlspecialchars( $elemts_texte ),
					] );

<<<<<<< HEAD

					//FICHIERS
					$created_at = date_create( $_POST['date_commande'] );
					$created_at = date_format( $created_at, 'dmy' );
					$ref_member = $id_membre;
					$filname = $_FILES['fichier']['name'];
				 	$filename_2 = $_FILES['fichier_2']['name'];
					$filename_3 = $_FILES['fichier_3']['name'];
					$tab = explode('.', $filname);
					$tab_2 = explode('.', $filename_2);
					$tab_3 = explode('.', $filename_3);
					$comm = DB::select ('SELECT max(id) as cmd FROM commandes');
					$com = $comm[0]['cmd'];
					$ext = $tab[count( $tab )-1];

					$ext_2 = $tab_2[count( $tab_2 )-1];
					$ext_3 = $tab_3[count( $tab_3 )-1];
					$camp = $_POST['nom_campagne'];
					$camp = str_replace(' ', '-', $camp);

					$ref_commande = 'cmd-Djo-' . $ref_member . '-' . $created_at;

					if($ext)  $filename = $ref_commande . '-' . $com . '-briefcomplet-1-' . $camp . '.' . $ext;
					if($ext_2)$filename_2 = $ref_commande.'-' . $com. '-briefcomplet-2-' . $camp . '.' . $ext_2;
					if($ext_3) $filename_3 = $ref_commande.'-' . $com . '-briefcomplet-3-' . $camp . '.' . $ext_3;


					move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename );
					DB::update( 'update commandes set fichier = :fichier WHERE id = '. $com , [ 'fichier' => $filename ]);



					move_uploaded_file( $_FILES['fichier_2']['tmp_name'], ROOT . 'public/files/' . $filename_2 );
					DB::update( 'update commandes set fichier_2 = :fichier_2 WHERE id = '. $com , [ 'fichier_2' => $filename_2 ]);

					move_uploaded_file( $_FILES['fichier_3']['tmp_name'], ROOT . 'public/files/' . $filename_3 );
					DB::update( 'update commandes set fichier_3 = :fichier_3 WHERE id = '. $com , [ 'fichier_3' => $filename_3 ]);


					header( 'Location: /commandes/editer/' . $id_COMD );
				}
				else{
					$this->view( 'commandes/editer', ['erreur' => $erreur, 'commandes' => $editCommande[0]] );
				}
=======
                //FICHIERS
                    $created_at = date_create( $_POST['date_commande'] );
                    $created_at = date_format( $created_at, 'dmy' );
                    $ref_member = $id_membre;
                    $filname = $_FILES['fichier']['name'];
                    $filename_2 = $_FILES['fichier_2']['name'];
                    $filename_3 = $_FILES['fichier_3']['name'];
                    $tab = explode('.', $filname);
                    $tab_2 = explode('.', $filename_2);
                    $tab_3 = explode('.', $filename_3);
                    $comm = DB::select ('SELECT max(id) as cmd FROM commandes');
                    $com = $comm[0]['cmd'];
                    $ext = $tab[count( $tab )-1];

                    $ext_2 = $tab_2[count( $tab_2 )-1];
                    $ext_3 = $tab_3[count( $tab_3 )-1];
                    $camp = $_POST['nom_campagne'];
                    $camp = str_replace(' ', '-', $camp);

                    $ref_commande = 'cmd-Djo-' . $ref_member . '-' . $created_at;

                    if($ext)  $filename = $ref_commande . '-' . $com . '-briefcomplet-1-' . $camp . '.' . $ext;
                    if($ext_2) $filename_2 = $ref_commande.'-' . $com. '-briefcomplet-2-' . $camp . '.' . $ext_2;
                    if($ext_3) $filename_3 = $ref_commande.'-' . $com . '-briefcomplet-3-' . $camp . '.' . $ext_3;


                    move_uploaded_file( $_FILES['fichier']['tmp_name'], ROOT . 'public/files/' . $filename );
                    DB::update( 'update commandes set fichier = :fichier WHERE id = '. $com , [ 'fichier' => $filename ]);

                    move_uploaded_file( $_FILES['fichier_2']['tmp_name'], ROOT . 'public/files/' . $filename_2 );
                    DB::update( 'update commandes set fichier_2 = :fichier_2 WHERE id = '. $com , [ 'fichier_2' => $filename_2 ]);

                    move_uploaded_file( $_FILES['fichier_3']['tmp_name'], ROOT . 'public/files/' . $filename_3 );
                    DB::update( 'update commandes set fichier_3 = :fichier_3 WHERE id = '. $com , [ 'fichier_3' => $filename_3 ]);

                    header( 'Location: /commandes' );
                }
                else{

                    $erreur[''] = 'Merci de renseigner les champs notifiés et de re-valider votre commande';
                }

                $this->view( 'commandes/editer', ['erreur' => $erreur, 'commandes' => $editCommande[0]] );

>>>>>>> 00b43c81e7c6e323cb4395549f38074a0a8680f8
			}
// 			var_dump($editCommande[0]);
			$this->view( 'commandes/editer', ['commandes' => $editCommande[0], 'membre' => $membre[0]] );
		}
        //SUPPRIMER UNE FORMULE
        function supprimer( int $idCommande ) {
            if( !isset( $_SESSION['id'] ) ) {
                header( 'Location: /member/compte' );
            }

            $commandes = DB::delete ( 'delete from commandes where id = ?', [$idCommande] );

            header( 'Location: /member/compte' );
        }
}
