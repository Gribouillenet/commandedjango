<?php
class Member extends Controller {

//AFFICHAGE DES FORMULES
	public function index() {
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /member/connexion' );
		}


		$formules = DB::select( 'SELECT * FROM formules ORDER BY id ASC' );

		$id_membre = $_SESSION['id'];
		$membre = DB::select ('SELECT * FROM member WHERE id = ?', [$id_membre]);



	  	foreach ( $formules as $key => $formule ) {

		  	$formules[$key]['body'] = nl2br( $formule['body'] );

		  	$nb = $formule['id'];

	  	}

	  	$this->view( 'formules/index', ['formules' => $formules] );

	}
//COMPTE

	public function compte(){

		$id = $_SESSION['id'];

        $commandes = DB::select( 'SELECT * FROM commandes, member, formules WHERE member.id = ? AND ref_Member = member.id AND
		ref_Formules = formules.id GROUP BY commandes.id ORDER BY commandes.id DESC', [$id] );

        $membre = DB::select( 'SELECT * FROM  member WHERE  member.id = ?', [$id] );

		if(!$commandes){
		  $commandes = DB::select( 'SELECT * FROM  commandes WHERE ref_Member.id = ? ', [$id] );
		}

		foreach ( $commandes as $key => $commande ){
 				$dateLivraison = date_create( $commande['date_livraison_client'] );
				$commandes[$key]['date_livraison_client'] = date_format( $dateLivraison, 'd/m/Y' );

				$dateCommande = date_create( $commande['date_commande'] );
				$commandes[$key]['date_commande'] = date_format( $dateCommande, 'dmy' );

				$commandes[$key]['observations'] = nl2br( $commande['observations'] );
				$commandes[$key]['elemts_texte'] = nl2br( $commande['elemts_texte'] );
			}

 		//var_dump($membre);

		$this->view( 'member/compte', ['account_commandes' => $commandes, 'account_membre' => $membre] );

	}

//CONNEXION
	public function connexion() {

	    if ( isset( $_SESSION['id'] ) ) {
	      header( 'Location: /member' );
	    }

	    if ( !empty( $_POST ) ) {
	      extract( $_POST );

	      $member = $this->accountExists();

			if ( $member ) {
				$_SESSION['id'] = $member['id'];

				header( 'Location: /formules' );
			}
			else {
				$erreur = 'Attention : Identifiants erronés ou manquants !';

			}

			$this->view( 'member/connexion', ['erreur' => $erreur] );
	    }

		$this->view( 'member/connexion' );
	}
//DECONNEXION
	public function deconnexion(){
		if( !isset( $_SESSION['id'] ) ) {
			header( 'Location: /member/connexion' );
		}

		$_SESSION = [];

		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		session_destroy();

		header( 'Location: /member/connexion' );
	}

//EDITER UN MOT DE PASSE
/*
	public function password( ) {

			$id = $_SESSION['id'];

		$member = DB::select( 'select * from member where id = ?', [$id] );


		if  ( !$member ){
			header( 'Location: /member' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];


			if( empty ( $password ) ) {
				$erreur['password'] = ' <br> password obligatoire';

			}

			      $password = password_hash($password, PASSWORD_DEFAULT );
				//echo"<br> update  member set password = '".$password."'  where id = ".$id;


			if( !$erreur) {
			 	DB::update( 'update  member set password = :password  where id = :id' , [
					'password' =>  $password ,
					'id' => $id
				] );
 				header( 'Location: /member/compte' );
			}

			else{
				$this->view( 'member/password', ['erreur' => $erreur, 'member' => $member[0]] );
			}
		}

		$this->view( 'member/password', ['member' => $member[0]] );

	}
*/
//ÉDITER SON COMPTE MEMBRE
	public function editer(  ) {

			$id = $_SESSION['id'];

		$member = DB::select( 'select * from member where id = ?', [$id] );


		if  ( !$member ){
			header( 'Location: /member' );
		}

		if ( !empty( $_POST ) ) {
			extract( $_POST );
			$erreur = [];

            if ( empty ( $name ) || empty( $tel_fixe ) || empty ( $tel_mob ) || empty ( $mail_member ) || empty ( $mail_delivery_member ) ) {
				$erreur['champ_obligatoire'] = '* Champs Obligatoires';
			}

			if( empty ( $name ) ) {
				$erreur['name'] = 'Le nom et prénom du commercial est obligatoire !';
			}

            function validerNumero($telATester) {
                //Retourne le numéro s'il est valide, sinon false.
                return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
            }

            if( empty ( $tel_fixe ) ) {
				$erreur['tel_fixe'] = 'Téléphone fixe obligatoire !';
			}

            if( !empty ( $tel_fixe ) ) {
                if ( !filter_var( $tel_fixe, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_fixe'] = 'Téléphone fixe incorrect.';
                }
			}

            if( empty ( $tel_mob ) ) {
				$erreur['tel_mob'] = 'Téléphone mobile obligatoire !';
			}

            if( !empty ( $tel_mob ) ) {
                if ( !filter_var( $tel_mob, FILTER_CALLBACK, array('options' => 'validerNumero') ) ) {
				    $erreur['tel_mob'] = 'Téléphone mobile incorrect.';
                }
			}

			if( empty ( $mail_member ) ) {
				$erreur['mail_member'] = 'L\'email est obligatoire !';
			}
			elseif ( !filter_var( $mail_member, FILTER_VALIDATE_EMAIL ) ){
				$erreur['mail_member'] = 'L\'adresse email est invalide !';
			}

            if( empty ( $mail_delivery_member ) ) {
				$erreur['mail_delivery_member'] = 'L\'email est obligatoire !';
			}
            if( !empty ( $mail_delivery_member ) ) {
                if ( !filter_var( $mail_delivery_member, FILTER_VALIDATE_EMAIL ) ){
                    $erreur['mail_delivery_member'] = 'L\'adresse email est invalide !';
                }
            }
			$since=date("Y-m-d H:m:s");

			if( !$erreur) {
			//echo'update  member set radio_name = '.$radio_name.', name ='.$name.', tel_fixe ='.$tel_fixe.', tel_mob ='.$tel_mob.', mail_member ='.$mail_member.',
			//	login ='.$login.' where id = '.$id.'' ;

				DB::update( 'update member set name = :name, tel_fixe = :tel_fixe, tel_mob = :tel_mob, mail_member = :mail_member, mail_delivery_member = :mail_delivery_member where id = :id' , [
					'name' => htmlspecialchars( $name ),
				    'tel_fixe' => htmlspecialchars( $tel_fixe ),
					'tel_mob' => htmlspecialchars( $tel_mob ),
					'mail_member' => htmlspecialchars( $mail_member ),
					'mail_delivery_member' => htmlspecialchars( $mail_delivery_member ),
					'id' => $id
				] );
 				header( 'Location: /member/compte' );
			}

			else{
				$this->view( 'member/editer', ['erreur' => $erreur, 'member' => $member[0]] );
			}
		}

		$this->view( 'member/editer', ['member' => $member[0]] );

	}


	private function accountExists() : array {
		$member = DB::select( 'SELECT * FROM member WHERE login = :login  and password = :password', [
								'login' =>  $_POST['login'],
								'password' =>  md5($_POST['password'])
							] );




		/*if ( $member && password_verify( $_POST['password'], $member[0]['password'] ) ) {
			return $member[0];
		}
		echo "<br> password_verify ".password_verify( $_POST['password'], $member[0]['password']);
		echo "<br> password_hash ".md5($_POST['password']);
		echo "<br> Test 123 ".password_hash("Test123", PASSWORD_DEFAULT );
		echo "<br> k2OR7JeQ :  ".md5("k2OR7JeQ" );
		echo "<pre>";
		print_r($member);
		exit;*/
		if ( $member){

		return $member[0];
		}
		else {
			return [];
		}
	}

}
